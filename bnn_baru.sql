-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 02, 2019 at 05:01 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bnn`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `s_ket` (`id_in` INT(11), `th` VARCHAR(10), `bln` VARCHAR(10)) RETURNS VARCHAR(30) CHARSET latin1 BEGIN
  declare last_key_user varchar(30);
  declare count_row_user int;
  declare fix_key_user varchar(30);
  
  select count(*) into count_row_user from tbl_periksa 
  	where nomer_surat != '0' and substr(nomer_surat,19,4) = th;
        
 
  select nomer_surat into last_key_user from tbl_periksa
  	where nomer_surat != '0' and substr(nomer_surat,19,4) = th
  	order by nomer_surat desc limit 1;
  
  if(count_row_user <1) then
  	set fix_key_user = concat("SKET/","000001/",bln,"/Ka/",th,"/Rehab");
  else
    set fix_key_user = concat("SKET/",LPAD((substr(last_key_user,6,6)+1), 6, '0'),"/",bln,"/Ka/",th,"/Rehab");
      
  END IF;
  
  update tbl_periksa set nomer_surat = fix_key_user, status = '1' where id = id_in;
  
  return fix_key_user;
  
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `id_lv` enum('0','1','2','3') NOT NULL,
  `status_active` enum('0','1') NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nip` varchar(64) NOT NULL,
  `foto_admin` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `admin_del` varchar(12) NOT NULL,
  `time_update` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `email`, `username`, `password`, `id_lv`, `status_active`, `nama`, `nip`, `foto_admin`, `is_delete`, `admin_del`, `time_update`) VALUES
(1, 'suryahanggarass@gmail.com', 'suryahanggara', '21232f297a57a5a743894a0e4a801fc3', '1', '1', 'Irana Dewi Mardika, S. Kep.Ns', '11111111111122', '20191016044513.jpg', '0', '0', '2019-07-24 01:47:32'),
(2, 'surya@gmail.com', 'surya', '21232f297a57a5a743894a0e4a801fc3', '0', '1', 'Tukiem, S. Kep.Ns', '11111111111122', '20191016044526.jpg', '0', '0', '2019-09-01 03:59:41'),
(4, 'donal@gmail.com', 'donal', '21232f297a57a5a743894a0e4a801fc3', '1', '1', 'donal trump', '11111111111122', '', '0', '0', '2019-10-15 04:45:59'),
(5, 'pelayanan1@gmail.com', 'pelayanan1', '21232f297a57a5a743894a0e4a801fc3', '1', '1', 'saru', '11111111111122', '', '0', '0', '2019-10-15 10:54:27'),
(6, 'pelayanan2@gmail.com', 'pelayanan2', '21232f297a57a5a743894a0e4a801fc3', '1', '1', 'dua', '11111111111122', '', '0', '0', '2019-10-15 10:55:31'),
(7, 'jend@gmail.com', 'sudirman', '21232f297a57a5a743894a0e4a801fc3', '1', '1', 'jendral sudirman s', '11111111111122', '20191016040804.jpg', '0', '0', '2019-10-16 04:45:47'),
(8, 'pelayanan3@gmail.com', 'pelayanan3', '21232f297a57a5a743894a0e4a801fc3', '1', '0', 'pelayanan dua', '11111111111122', '20191016044300.jpg', '0', '0', '2019-10-16 04:43:00'),
(9, 'ok@gmail.com', 'ok', '21232f297a57a5a743894a0e4a801fc3', '1', '0', 'okkookko', '11111111111122', '20191016064500.jpg', '0', '0', '2019-10-16 06:45:00');

-- --------------------------------------------------------

--
-- Table structure for table `data_pasien`
--

CREATE TABLE `data_pasien` (
  `id_data` int(10) NOT NULL,
  `id_pasien` int(20) NOT NULL,
  `jenis_identitas` varchar(50) NOT NULL,
  `no_identitas` varchar(13) NOT NULL,
  `foto` varchar(20) NOT NULL,
  `no_hp` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `jkel` varchar(10) NOT NULL,
  `tempat` varchar(50) NOT NULL,
  `tanggal_lahir` int(20) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `desa` varchar(50) NOT NULL,
  `kec` varchar(20) NOT NULL,
  `kab` varchar(50) NOT NULL,
  `pekerjaan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_pasien_new`
--

CREATE TABLE `data_pasien_new` (
  `id_data` int(11) NOT NULL,
  `id_pasien` int(11) NOT NULL,
  `tgl_daftar` date NOT NULL,
  `jenis_identitas` enum('0','1','2') NOT NULL,
  `no_identitas` varchar(20) NOT NULL,
  `foto` text NOT NULL,
  `no_hp` varchar(13) NOT NULL,
  `nama` varchar(64) NOT NULL,
  `alamat` text NOT NULL,
  `jk` enum('0','1') NOT NULL,
  `tmp_lhr` varchar(64) NOT NULL,
  `tgl_lhr` date NOT NULL,
  `kel` varchar(10) NOT NULL,
  `kec` varchar(10) NOT NULL,
  `kab` varchar(10) NOT NULL,
  `pekerjaan` text NOT NULL,
  `sts_periksa` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_pasien_new`
--

INSERT INTO `data_pasien_new` (`id_data`, `id_pasien`, `tgl_daftar`, `jenis_identitas`, `no_identitas`, `foto`, `no_hp`, `nama`, `alamat`, `jk`, `tmp_lhr`, `tgl_lhr`, `kel`, `kec`, `kab`, `pekerjaan`, `sts_periksa`) VALUES
(4, 0, '2019-09-05', '2', '3573011653542221', '20190905011835.jpg', '081230695771', 'Surya Hanggaraa', 'Malang Selatan', '1', 'Malang Selatan', '2000-09-06', '3', '3', '1', 'Preman P', '1'),
(5, 0, '2019-09-06', '1', '1234567891234566', '20190905022204.jpg', '081230695744', 'ara', 'Malang', '0', 'Malang', '2000-09-02', '1', '1', '1', 'preman', '1'),
(6, 0, '2019-09-08', '0', '213213142', '20190908023710.jpg', '8123069554', 'asdasdsad', 'Malang', '0', 'malang', '2000-09-09', '4', '4', '2', 'tau ah', '1'),
(7, 0, '2019-09-08', '0', '3572066695845557', '20190908072557.jpg', '081230697777', 'toko mbah surip', 'Malang', '0', 'malang', '2019-09-03', '4', '10', '2', 'maling', '1'),
(8, 0, '2019-09-10', '0', '123465798465123', '20190910150337.jpg', '081230695774', 'ara', 'malang', '0', 'malang', '2019-09-10', '8', '4', '2', 'ara', '1');

-- --------------------------------------------------------

--
-- Table structure for table `db_kab`
--

CREATE TABLE `db_kab` (
  `id_kab` int(20) NOT NULL,
  `kab` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db_kab`
--

INSERT INTO `db_kab` (`id_kab`, `kab`) VALUES
(1, 'Kabupaten Blitar'),
(2, 'Kota Blitar');

-- --------------------------------------------------------

--
-- Table structure for table `db_kec`
--

CREATE TABLE `db_kec` (
  `id_kec` int(11) NOT NULL,
  `kecamatan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db_kec`
--

INSERT INTO `db_kec` (`id_kec`, `kecamatan`) VALUES
(1, 'Bakung'),
(2, 'Binangun'),
(3, 'Doko'),
(4, 'Kesamben'),
(5, 'Kanigoro'),
(6, 'Kademangan'),
(7, 'Nglegok'),
(8, 'Sanankulon'),
(9, 'Selorejo'),
(10, 'Sutojayan'),
(11, 'Selopuro'),
(12, 'Talun'),
(13, 'Udanawu'),
(14, 'Ponggok'),
(15, 'Srengat'),
(16, 'Wonodadi'),
(17, 'Wlingi'),
(18, 'Wates'),
(19, 'Wonotirto'),
(20, 'Gandusari'),
(21, 'Garum'),
(22, 'Panggurejo'),
(23, 'Kepanjen Kidul'),
(24, 'Sanan Wetan'),
(25, 'Sukorejo');

-- --------------------------------------------------------

--
-- Table structure for table `db_kelurahan`
--

CREATE TABLE `db_kelurahan` (
  `id_kel` int(20) NOT NULL,
  `id_kec` int(20) NOT NULL,
  `kelurahan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db_kelurahan`
--

INSERT INTO `db_kelurahan` (`id_kel`, `id_kec`, `kelurahan`) VALUES
(1, 1, 'Bakung'),
(2, 1, 'Bululawang'),
(3, 1, 'Kedungbanteng'),
(4, 1, 'Lorejo'),
(5, 1, 'Ngrejo'),
(6, 1, 'Plandirejo'),
(7, 1, 'Pulerejo'),
(8, 2, 'Binangun'),
(9, 2, 'Birowo'),
(10, 2, 'Kedungwungu'),
(11, 2, 'Ngadri'),
(12, 2, 'Ngembul'),
(13, 2, 'Rejoso'),
(14, 2, 'Sambigede'),
(15, 2, 'Salamrejo'),
(16, 2, 'Sukorame'),
(17, 2, 'Sumber Kembar'),
(18, 2, 'Tawang Rejo'),
(19, 2, 'Umbul Damar'),
(20, 1, 'Sidomulyo'),
(21, 1, 'Sumberdadi'),
(22, 1, 'Tumpakepuh'),
(23, 1, 'Tumpakoyot'),
(24, 3, 'Doko'),
(25, 3, 'Genengan'),
(26, 3, 'Jambepawon'),
(27, 3, 'Kalimanis'),
(28, 3, 'Plumbangan'),
(29, 3, 'Resapombo'),
(30, 3, 'Sidorejo'),
(31, 3, 'Slorok'),
(32, 3, 'Sumber Urip'),
(33, 3, 'Suru'),
(39, 4, 'Bumirejo'),
(40, 4, 'Jugo'),
(41, 4, 'Kemirigede'),
(42, 4, 'Kesamben'),
(43, 4, 'Pagergunung'),
(44, 4, 'Pagerwojo'),
(45, 4, 'Siraman'),
(46, 4, 'Sukoanyar'),
(47, 4, 'Tapakrejo'),
(48, 4, 'Tepas'),
(49, 5, 'Banggle'),
(50, 5, 'Gaprang'),
(51, 5, 'Gogodeso'),
(52, 5, 'Jatinom'),
(53, 5, 'Kanigoro'),
(54, 5, 'Karangsono'),
(55, 5, 'Kuningan'),
(56, 5, 'Minggirsari'),
(57, 5, 'Papungan'),
(58, 5, 'Satreyan'),
(59, 5, 'Sawentar'),
(60, 5, 'Tlogo'),
(61, 6, 'Bendosari'),
(62, 6, 'Darungan'),
(63, 6, 'Dawuhan'),
(64, 6, 'Jimbe'),
(65, 6, 'Kademangan'),
(66, 6, 'Kebonsari'),
(67, 6, 'Maron'),
(68, 6, 'Pakisaji'),
(69, 6, 'Panggungduwet'),
(70, 6, 'Plosorejo'),
(71, 6, 'Plumpungrejo'),
(72, 6, 'Rejowinangun'),
(73, 6, 'Sumberjati'),
(74, 6, 'Sumberjo'),
(75, 6, 'Suruhwadang'),
(76, 7, 'Bangsri'),
(77, 7, 'Dayu'),
(78, 7, 'Jiwut'),
(79, 7, 'Kedawung'),
(80, 7, 'Kemloko'),
(81, 7, 'Krenceng'),
(82, 7, 'Modangan'),
(83, 7, 'Nglegok'),
(84, 7, 'Ngoran'),
(85, 7, 'Penataran'),
(86, 7, 'Sumberasri'),
(87, 8, 'Bendosari'),
(88, 8, 'Bendowulung'),
(89, 8, 'Gledug'),
(90, 8, 'Jeding'),
(91, 8, 'Kalipucung'),
(92, 8, 'Plosoarang'),
(93, 8, 'Purworejo'),
(94, 8, 'Sanankulon'),
(95, 8, 'Sumber'),
(96, 8, 'Sumberingin'),
(97, 8, 'Sumberjo'),
(98, 8, 'Tuliskriyo'),
(99, 9, 'Ampelgading'),
(100, 9, 'Banjarsari'),
(101, 9, 'Boro'),
(102, 9, 'Ngreco'),
(103, 9, 'Ngrendeng'),
(104, 9, 'Olan Alen'),
(105, 9, 'Pohgajih'),
(106, 9, 'Selorejo'),
(107, 9, 'Sidomulyo'),
(108, 9, 'Sumber Agung'),
(109, 10, 'Bacem'),
(110, 10, 'Jegu'),
(111, 10, 'Jingglong'),
(112, 10, 'Kalipang'),
(113, 10, 'Kalipang'),
(114, 10, 'Kaulon'),
(115, 10, 'Kedung Bunder'),
(116, 10, 'Kembangarum'),
(117, 10, 'Pandanarum'),
(118, 10, 'Sutojayan'),
(119, 10, 'Sukorejo'),
(120, 10, 'Sumberjo'),
(121, 11, 'Jambewangi'),
(122, 11, 'Jatitengah'),
(123, 11, 'Mandesan'),
(124, 11, 'Mronjo'),
(125, 11, 'Ploso'),
(126, 11, 'Popoh'),
(127, 11, 'Selopuro'),
(128, 11, 'Tegalrejo'),
(129, 12, 'Bajang'),
(130, 12, 'Bendosewu'),
(131, 12, 'Duren'),
(132, 12, 'Jabung'),
(133, 12, 'Jajar'),
(134, 12, 'Jeblog'),
(135, 12, 'Kamulan'),
(136, 12, 'Kaweron'),
(137, 12, 'Kendalrejo'),
(138, 12, 'Pasirharjo'),
(139, 12, 'Sragi'),
(140, 12, 'Talun'),
(141, 12, 'Tumpang'),
(142, 12, 'Wonorejo'),
(143, 13, 'Bakung'),
(144, 13, 'Bendorejo'),
(145, 13, 'Besuki'),
(146, 13, 'Jati'),
(147, 13, 'Karanggondang'),
(148, 13, 'Mangunan'),
(149, 13, 'Ringinanom'),
(150, 13, 'Slemanan'),
(151, 13, 'Sukorejo'),
(152, 13, 'Sumbersari'),
(153, 13, 'Temenggunang'),
(154, 13, 'Tunjung'),
(155, 14, 'Bacem'),
(156, 14, 'Bendo'),
(157, 14, 'Candirejo'),
(158, 14, 'Dadaplangu'),
(159, 14, 'Gembongan'),
(160, 14, 'Jatilengger'),
(161, 14, 'Karangbendo'),
(162, 14, 'Kawedusan'),
(163, 14, 'Kebonduren'),
(164, 14, 'Langon'),
(165, 14, 'Maliran'),
(166, 14, 'Pojok'),
(167, 14, 'Ponggok'),
(168, 14, 'Ringinanyar'),
(169, 14, 'Sidorejo'),
(170, 15, 'Bagelenan'),
(171, 15, 'Dandong'),
(172, 15, 'Dermojayan'),
(173, 15, 'Kandangan'),
(174, 15, 'Karanggayam'),
(175, 15, 'Kauman'),
(176, 15, 'Kendalrejo'),
(177, 15, 'Kerjen'),
(178, 15, 'Maron'),
(179, 15, 'Ngaglik'),
(180, 15, 'Pakisrejo'),
(181, 15, 'Purwokerto'),
(182, 15, 'Selokajang'),
(183, 15, 'Srengat'),
(184, 15, 'Togongan'),
(185, 15, 'Wonorejo'),
(186, 16, 'Gandekan'),
(187, 16, 'Jaten'),
(188, 16, 'Kaliboto'),
(189, 16, 'Kebonagung'),
(190, 16, 'Kolomayan'),
(191, 16, 'Kunir'),
(192, 16, 'Pikatan'),
(193, 16, 'Rejosari'),
(194, 16, 'Salam'),
(195, 16, 'Tawangrejo'),
(196, 16, 'Wonodadi'),
(197, 17, 'Babadan'),
(198, 17, 'Balerejo'),
(199, 17, 'Beru'),
(200, 17, 'Klemunan'),
(201, 17, 'Ngadirenggo'),
(202, 17, 'Tangkil'),
(203, 17, 'Tegalasri'),
(204, 17, 'Tembalang'),
(205, 17, 'Wlingi'),
(206, 18, 'Mojorejo'),
(207, 18, 'Purworejo'),
(208, 18, 'Ringin Rejo'),
(209, 18, 'Sukorejo'),
(210, 18, 'Sumberarum'),
(211, 18, 'Tugu Rejo'),
(212, 18, 'Tulungrejo'),
(213, 18, 'Wates'),
(214, 19, 'Gunung Gede'),
(215, 19, 'Kaligrenjeng'),
(216, 19, 'Ngadipuro'),
(217, 19, 'Ngeni'),
(218, 19, 'Pasiraman'),
(219, 19, 'Sumberboto'),
(220, 19, 'Tambakrejo'),
(221, 19, 'Wonotirto'),
(222, 20, 'Butun'),
(223, 20, 'Gadungan'),
(224, 20, 'Gandusari'),
(225, 20, 'Gondang'),
(226, 20, 'Kotes'),
(227, 20, 'Krisik'),
(228, 20, 'Ngaringan'),
(229, 20, 'Semen'),
(230, 20, 'Slumbung'),
(231, 20, 'Soso'),
(232, 20, 'Sukosewu'),
(233, 20, 'Sumber Agung'),
(234, 20, 'Tambakan'),
(235, 20, 'Tulungrejo'),
(236, 21, 'Bence'),
(237, 21, 'Garum'),
(238, 21, 'Karangrejo'),
(239, 21, 'Pojok'),
(240, 21, 'Sidodadi'),
(241, 21, 'Slorok'),
(242, 21, 'Sumberdiren'),
(243, 21, 'Tawang Sari'),
(244, 21, 'Tingal'),
(245, 22, 'Bale Rejo'),
(246, 22, 'Bumiayu'),
(247, 22, 'Kali Gambar'),
(248, 22, 'Kalitengah'),
(249, 22, 'Margo Mulyo'),
(250, 22, 'Panggung Asri'),
(251, 22, 'Panggung Rejo'),
(252, 22, 'Serang'),
(253, 22, 'Sumber Agung'),
(254, 22, 'Sumbersih'),
(255, 23, 'Sentul'),
(256, 23, 'Ngadirejo'),
(257, 23, 'Tanggung'),
(258, 23, 'Bendo'),
(259, 23, 'Kauman'),
(260, 23, 'Kepanjen Kidul'),
(261, 23, 'Kepanjen Lor'),
(262, 24, 'Bendogerit'),
(263, 24, 'Plosokerep'),
(264, 24, 'Rembang'),
(265, 24, 'Klampok'),
(266, 24, 'Gedog'),
(267, 24, 'Karang Tengah'),
(268, 24, 'Sanan Wetan'),
(269, 25, 'Sukorejo'),
(270, 25, 'Pakunden'),
(271, 25, 'Tlumpu'),
(272, 25, 'Karang sari'),
(273, 25, 'Blitar'),
(274, 25, 'Tanjung Sari'),
(275, 25, 'Turi');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pejabat`
--

CREATE TABLE `tbl_pejabat` (
  `id_pejabat` int(11) NOT NULL,
  `tipe_pejabat` enum('0','1','2','3') NOT NULL,
  `nip_pejabat` varchar(32) NOT NULL,
  `nama_pejabat` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pejabat`
--

INSERT INTO `tbl_pejabat` (`id_pejabat`, `tipe_pejabat`, `nip_pejabat`, `nama_pejabat`) VALUES
(3, '1', '1231314123213213123', 'dr. Desy Triyanawati'),
(4, '1', '41387216321', 'dr. Ara Merdeka Sekali'),
(5, '2', '344556732541', 'Bagus Hari Cahyono, S.E.'),
(6, '3', '0092718239123', 'Drs. Djoko Santoso');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_periksa`
--

CREATE TABLE `tbl_periksa` (
  `id` int(11) NOT NULL,
  `waktu` datetime NOT NULL,
  `bertempat_dari` varchar(50) NOT NULL,
  `permintaan_dari` varchar(50) NOT NULL,
  `srt_pengantar` text NOT NULL,
  `nomer_surat` varchar(50) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `id_pasien` varchar(50) NOT NULL,
  `kesadaran` varchar(100) NOT NULL,
  `keumum` varchar(100) NOT NULL,
  `tekanandarah` varchar(100) NOT NULL,
  `nadi` varchar(100) NOT NULL,
  `pernafasan` varchar(100) NOT NULL,
  `penggunaanobat` varchar(100) NOT NULL,
  `jenisobat` varchar(50) NOT NULL,
  `asalobat` varchar(50) NOT NULL,
  `terakirminum` varchar(50) NOT NULL,
  `hasiltes` varchar(100) NOT NULL,
  `metode` varchar(100) NOT NULL,
  `amphe` varchar(50) NOT NULL,
  `metha` varchar(50) NOT NULL,
  `coco` varchar(50) NOT NULL,
  `opioid` varchar(50) NOT NULL,
  `thc` varchar(50) NOT NULL,
  `benzo` varchar(50) NOT NULL,
  `k2` varchar(50) NOT NULL,
  `lain` varchar(50) NOT NULL,
  `keperluan` varchar(100) NOT NULL,
  `id_pejabat` varchar(50) NOT NULL,
  `id_admin_layanan` varchar(50) NOT NULL,
  `mengetahui` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_periksa`
--

INSERT INTO `tbl_periksa` (`id`, `waktu`, `bertempat_dari`, `permintaan_dari`, `srt_pengantar`, `nomer_surat`, `status`, `id_pasien`, `kesadaran`, `keumum`, `tekanandarah`, `nadi`, `pernafasan`, `penggunaanobat`, `jenisobat`, `asalobat`, `terakirminum`, `hasiltes`, `metode`, `amphe`, `metha`, `coco`, `opioid`, `thc`, `benzo`, `k2`, `lain`, `keperluan`, `id_pejabat`, `id_admin_layanan`, `mengetahui`) VALUES
(6, '2019-09-08 02:51:30', 'BNNK Blitar', 'mana ku tau mana ku tau', '451151asd1/da51d6/asd561', 'SKET/000001/09/Ka/2019/Rehab', '1', '4', 'terganggu', 'cukup', 'mana ku tau mana ku tau', 'mana ku tau mana ku tau', 'mana ku tau mana ku tau', 'tidak_ada', 'mana ku tau mana ku tau', 'pembelian bebas', 'mana ku tau mana ku tau', 'negatif', 'Rapid Test', 'negatif', 'negatif', 'negatif', 'negatif', 'negatif', 'negatif', 'negatif', 'mana ku tau mana ku tau', 'mana ku tau mana ku tau', '3', '1', '0'),
(7, '2019-09-08 03:55:30', 'BNNK Blitar', 'Cermati', '451151asd1/da51d6/asd561', 'SKET/000002/09/Ka/2019/Rehab', '1', '5', 'terganggu', 'cukup', 'Cermati', 'Cermati', 'Cermati', 'tidak_ada', 'Cermati', 'resep dokter', 'Cermati', 'positif', 'Rapid Test', 'positif', 'negatif', 'positif', 'negatif', 'positif', 'negatif', 'positif', '', 'Cermati', '3', '1', '0'),
(9, '2019-09-08 04:13:13', 'BNNK Blitar', 'Terperiksa', '451151asd1/da51d6/asd561', 'SKET/000003/09/Ka/2019/Rehab', '1', '6', 'baik', 'baik', 'Terperiksa', 'Terperiksa', 'Terperiksa', 'ada', 'Terperiksa', 'resep dokter', 'Terperiksa', 'positif', 'Rapid Test', 'negatif', 'negatif', 'negatif', 'negatif', 'negatif', 'negatif', 'negatif', '', 'Terperiksa', '3', '1', '0'),
(11, '2019-09-08 15:34:50', 'BNNK Blitar', 'toko mbah surip', '451151asd1/da51d6/asd561', 'SKET/000004/09/Ka/2019/Rehab', '1', '7', 'baik', 'baik', 'toko mbah surip', 'toko mbah surip', 'toko mbah surip', 'ada', 'toko mbah surip', 'resep dokter', 'toko mbah surip', 'positif', 'Rapid Test', 'negatif', 'negatif', 'negatif', 'negatif', 'negatif', 'negatif', 'negatif', '', 'toko mbah surip', '3', '1', '0'),
(12, '2019-09-10 17:24:04', 'BNNK Blitar', 'malang', '451151asd1/da51d6/asd561', 'SKET/000005/09/Ka/2019/Rehab', '1', '8', 'baik', 'baik', 'malang', 'malang', 'malang', 'ada', 'sadsads', 'resep dokter', 'dadsadsa', 'positif', 'Rapid Test', 'negatif', 'negatif', 'negatif', 'negatif', 'negatif', 'negatif', 'negatif', 'malang', 'malang', '3', '1', '0'),
(16, '2019-10-15 10:31:08', 'BNNK Blitar', 'dasdsa', '451151asd1/da51d6/asd561', '0', '0', '5', 'baik', 'baik', 'dasdsadsad', 'sadsa', 'dsadsadsad', 'tidak_ada', '-', '-', '-', 'positif', 'Rapid Test', 'positif', 'positif', 'positif', 'positif', 'positif', 'positif', 'positif', 'sadsadsa', 'dsadsad', '3', '1', '1'),
(18, '2019-10-16 07:53:01', 'BNNK Blitar', 'dasdnsajno xx', 'a/21312/sdfs/23131xx', 'SKET/000006/10/Ka/2019/Rehab', '1', '8', 'baik', 'baik', 'dasjknfkjdsnxx', 'kdsjnfkjdsn sdnfkdsjnfkj dsjfnkdjsnxx', 'djknfksdjnfsdfkndsxx', 'tidak_ada', '-', '-', '-', 'positif', 'Rapid Test', 'positif', 'positif', 'positif', 'positif', 'positif', 'positif', 'positif', 'fsdvsddsv sd svdsvdsvdsxx', 'psadjnsakjbxx', '3', '1', '0'),
(19, '2019-10-27 09:42:15', 'BNNK Blitar', 'Universitas Brawijaya', '451151asd1/da51d6/asd561', 'SKET/000007/10/Ka/2019/Rehab', '1', '8', 'baik', 'baik', 'normal', 'normal', 'sehat', 'tidak_ada', '-', 'resep dokter', '-', 'positif', 'Rapid Test', 'positif', 'positif', 'positif', 'positif', 'positif', 'positif', 'positif', '', 'Pendaftaran Bintara', '4', '4', '1'),
(20, '2019-10-31 02:39:40', 'BNNK Blitar', 'dasdsa', 'sadsad/sadsadsa/dsadsadsad/sadsad', 'SKET/000008/10/Ka/2019/Rehab', '1', '8', 'baik', 'baik', 'sadsad', 'sadsad', 'dasdsadsad', 'tidak_ada', '-', '-', '-', 'positif', 'Rapid Test', 'positif', 'positif', 'positif', 'positif', 'positif', 'positif', 'positif', '', 'dasdsadasd', '3', '4', '0'),
(21, '2019-10-31 03:41:49', 'BNNK Blitar', 'sadsad', 'asdsad/dasd/sad', 'SKET/000009/10/Ka/2019/Rehab', '1', '6', 'baik', 'baik', 'asdsad', 'asdsadsa', 'sadsadsad', 'tidak_ada', '-', '-', '-', 'positif', 'Rapid Test', 'positif', 'positif', 'positif', 'positif', 'positif', 'positif', 'positif', 'dasdsad', 'dsadsadsad', '3', '4', '0'),
(23, '2019-10-31 03:46:49', 'BNNK Blitar', 'dsadsad sad sad', 'dsads/asd/asd/sad/dasd', 'SKET/000010/10/Ka/2019/Rehab', '1', '6', 'baik', 'baik', 'sahdjha sdhasgd', 'ahdsahjd hsajgd h', 'asdhgasjhdg ', 'ada', 'hasgdh j', 'resep dokter', 'hdagsdjh sga', 'positif', 'Rapid Test', 'positif', 'positif', 'positif', 'positif', 'positif', 'positif', 'positif', 'adsdsad', 'asdsadgasjhdv ', '3', '4', '0'),
(24, '2019-10-31 10:08:43', 'BNNK Blitar', 'adsad', 'aasdsa/d/asdsad/asd', 'SKET/000011/10/Ka/2019/Rehab', '1', '8', 'baik', 'baik', 'adjhkajshd ajsdhjksa d', 'ajdhksjahd ', 'adjhsajdh jhsadkj', 'tidak_ada', '-', '-', '-', 'positif', 'Rapid Test', 'positif', 'positif', 'positif', 'positif', 'positif', 'positif', 'positif', 'adjskdba', 'adsadhjg ajsdk', '3', '4', '0'),
(25, '2019-11-02 10:58:01', 'BNNK Blitar', 'dasdsa', 'dsad', 'SKET/000012/11/Ka/2019/Rehab', '1', '6', 'baik', 'baik', 'asdsad', 'asdsadsa', 'dasd', 'tidak_ada', '-', '-', '-', 'positif', 'Rapid Test', 'positif', 'positif', 'positif', 'positif', 'positif', 'positif', 'positif', 'dasdsadasd', 'sadsadsad', '3', '1', '0');

-- --------------------------------------------------------

--
-- Table structure for table `welcom`
--

CREATE TABLE `welcom` (
  `id` int(50) NOT NULL,
  `nama` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `data_pasien`
--
ALTER TABLE `data_pasien`
  ADD PRIMARY KEY (`id_data`);

--
-- Indexes for table `data_pasien_new`
--
ALTER TABLE `data_pasien_new`
  ADD PRIMARY KEY (`id_data`);

--
-- Indexes for table `db_kab`
--
ALTER TABLE `db_kab`
  ADD PRIMARY KEY (`id_kab`);

--
-- Indexes for table `db_kec`
--
ALTER TABLE `db_kec`
  ADD PRIMARY KEY (`id_kec`);

--
-- Indexes for table `db_kelurahan`
--
ALTER TABLE `db_kelurahan`
  ADD PRIMARY KEY (`id_kel`);

--
-- Indexes for table `tbl_pejabat`
--
ALTER TABLE `tbl_pejabat`
  ADD PRIMARY KEY (`id_pejabat`);

--
-- Indexes for table `tbl_periksa`
--
ALTER TABLE `tbl_periksa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `welcom`
--
ALTER TABLE `welcom`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `data_pasien`
--
ALTER TABLE `data_pasien`
  MODIFY `id_data` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_pasien_new`
--
ALTER TABLE `data_pasien_new`
  MODIFY `id_data` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `db_kab`
--
ALTER TABLE `db_kab`
  MODIFY `id_kab` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `db_kec`
--
ALTER TABLE `db_kec`
  MODIFY `id_kec` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `db_kelurahan`
--
ALTER TABLE `db_kelurahan`
  MODIFY `id_kel` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=276;

--
-- AUTO_INCREMENT for table `tbl_pejabat`
--
ALTER TABLE `tbl_pejabat`
  MODIFY `id_pejabat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_periksa`
--
ALTER TABLE `tbl_periksa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `welcom`
--
ALTER TABLE `welcom`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
