<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Beranda <?php print_r($status_admin);?></h3>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <?php
        if(isset($_SESSION["admin_lv_1"])){
            $name = $_SESSION["admin_lv_1"]["nama"];
            $username = $_SESSION["admin_lv_1"]["username"];
            $email = $_SESSION["admin_lv_1"]["email"];
        }

        if(isset($_SESSION["admin_lv_1"])){
            $status_admin = "Admin Super";
            if($_SESSION["admin_lv_1"]["id_lv"] == "1"){
                $status_admin = "Admin Pelayanan";
            }
        }
    ?>

    <div class="row">
        <!-- Column -->
        <div class="col-lg-4">
            <div class="card card-inverse card-success">
                <div class="card-body">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            <div class="carousel-item flex-column carousel-item-next carousel-item-left">
                                <i class="fa fa-users fa-2x text-white"></i>
                                <a href="">
                                    <h3 class="text-white font-light">Data <span class="font-bold">Pasien</span><br>&nbsp;</h3>    
                                </a>
                                <div class="text-white m-t-20">
                                    <i><?=$count_pasien;?> Orang</i>
                                </div>
                            </div>
                            <div class="carousel-item flex-column">
                                <i class="fa fa-users fa-2x text-white"></i>
                                <a href="">
                                    <h3 class="text-white font-light">Data <span class="font-bold">Pasien</span><br>&nbsp;</h3>
                                </a>
                                <div class="text-white m-t-20">
                                    <i><?=$count_pasien;?> Orang</i>
                                </div>
                            </div>
                            <div class="carousel-item flex-column active carousel-item-left">
                                <i class="fa fa-users fa-2x text-white"></i>
                                <a href="">
                                    <h3 class="text-white font-light">Data <span class="font-bold">Pasien</span><br>&nbsp;</h3>
                                </a>
                                <div class="text-white m-t-20">
                                    <i><?=$count_pasien;?> Orang</i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-4">
            <div class="card card-inverse card-info">
                <div class="card-body">
                    <div id="myCarouse2" class="carousel slide" data-ride="carousel">
                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            <div class="carousel-item flex-column carousel-item-next carousel-item-left">
                                <i class="fa fa-telegram fa-2x text-white"></i>
                                <a href="">
                                    <h3 class="text-white font-light">Pemeriksaan <span class="font-bold">Sudah</span><br>Di Setujui</h3>
                                </a>
                                <div class="text-white m-t-20">
                                    <i><?=$count_periksa_done;?> Pemeriksaan</i>
                                </div>
                            </div>
                            <div class="carousel-item flex-column">
                                <i class="fa fa-telegram fa-2x text-white"></i>
                                <a href="">
                                    <h3 class="text-white font-light">Pemeriksaan <span class="font-bold">Sudah</span><br>Di Setujui</h3>
                                </a>
                                <div class="text-white m-t-20">
                                    <i><?=$count_periksa_done;?> Pemeriksaan</i>
                                </div>
                            </div>
                            <div class="carousel-item flex-column active carousel-item-left">
                                <i class="fa fa-telegram fa-2x text-white"></i>
                                <a href="">
                                    <h3 class="text-white font-light">Pemeriksaan <span class="font-bold">Sudah</span><br>Di Setujui</h3>
                                </a>
                                <div class="text-white m-t-20">
                                    <i><?=$count_periksa_done;?> Pemeriksaan</i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-4">
            <div class="card card-inverse card-danger">
                <div class="card-body">
                    <div id="myCarouse3" class="carousel slide" data-ride="carousel">
                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            <div class="carousel-item flex-column carousel-item-next carousel-item-left">
                                <i class="fa fa-window-close fa-2x text-white"></i>
                                <a href="">
                                    <h3 class="text-white font-light">Pemeriksaan <span class="font-bold">Belum</span><br>Di Setujui</h3>
                                </a>
                                <div class="text-white m-t-20">
                                    <i><?=$count_periksa_fail;?> Pemeriksaan</i>
                                </div>
                            </div>
                            <div class="carousel-item flex-column">
                                <i class="fa fa-window-close fa-2x text-white"></i>
                                <a href="">
                                    <h3 class="text-white font-light">Pemeriksaan <span class="font-bold">Belum</span><br>Di Setujui</h3>
                                </a>
                                <div class="text-white m-t-20">
                                    <i><?=$count_periksa_fail;?> Pemeriksaan</i>
                                </div>
                            </div>
                            <div class="carousel-item flex-column active carousel-item-left">
                                <i class="fa fa-window-close fa-2x text-white"></i>
                                <a href="">
                                    <h3 class="text-white font-light">Pemeriksaan <span class="font-bold">Belum</span><br>Di Setujui</h3>
                                </a>
                                <div class="text-white m-t-20">
                                    <i><?=$count_periksa_fail;?> Pemeriksaan</i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>

    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-inverse card-info">
                <div class="card-body">
                    <h3 class="card-title">Selamat Datang, <?=$status_admin;?>, <?=$name;?> </h3>
                    <p class="card-text">Untuk memulai pemeriksaan, silahakan klik tombol di bawah ini.</p>
                    <a href="<?=base_url();?>admin/data/periksa" class="btn btn-inverse">Klik disini</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->

    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

<script type="text/javascript">
    $(document).ready(function(){
        console.log("ok");
    });

    function get_detail(id, t_produk_user, t_kontak_user, t_pembelian_user, t_pengeluaran_user, t_penjualan_user, t_hutang_user, t_piutang_user){
        $("#modal_detail_user").modal("show");
        console.log(id);

        $("#count_produk").html("<center>"+t_produk_user + " Data"+"</center>");
        $("#count_kontak").html("<center>"+t_kontak_user+ " Data"+"</center>");
        $("#count_pembelian").html("<center>"+t_pembelian_user+ " Data"+"</center>");
        $("#count_penjualan").html("<center>"+t_penjualan_user+ " Data"+"</center>");
        $("#count_pengeluaran").html("<center>"+t_pengeluaran_user+ " Data"+"</center>");
        $("#count_hutang").html("<center>"+t_hutang_user+ " Data"+"</center>");
        $("#count_piutang").html("<center>"+t_piutang_user+ " Data"+"</center>");
    }
</script>