<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Data Pemeriksaan</h3>
    </div>

</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Form Data Pemeriksaan</h4>

                </div>
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="row">
                        <!-- data_awal -->
                            <div class="col-md-12">
                                <h5><b>Data Identitas Terperiksa</b></h5>
                                <hr>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Pasien</label>
                                    <!-- <input type="text" class="form-control" id="id_pasien" name="id_pasien" required=""> -->
                                    <select class="form-control" id="id_pasien" name="id_pasien">
                                        <?php
                                        if($pasien){
                                            foreach ($pasien as $key => $value) {
                                                print_r("<option value=\"".$value->id_data."\">(".$value->id_data.") - ".$value->nama."</option>");
                                            }
                                        }
                                        ?>
                                    </select>
                                    <p id="msg_id_pasien" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Foto</label>
                                    <!-- <input type="file" class="form-control" id="foto" name="foto" required="">
                                    <p id="msg_foto" style="color: red;"></p><br> -->
                                    <center>
                                        <img src="" id="img_foto" style="width:162px ;height:237px ;">
                                    </center>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="row">
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">No. Identitas</label>
                                            <p><i><b><h5 id="nik">Default</h5></b></i></p>
                                            <!-- <input type="number" class="form-control" id="no_identitas" name="no_identitas" required="">
                                            <p id="msg_no_identitas" style="color: red;"></p> -->
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">Nama Pemohon</label>
                                            <p><i><b><h5 id="nama">Default</h5></b></i></p>
                                            <!-- <input type="text" class="form-control" id="nama" name="nama" required="">
                                            <p id="msg_nama" style="color: red;"></p> -->
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">Jenis Kelamin</label>
                                            <p><i><b><h5 id="jk">Default</h5></b></i></p>
                                            <!-- <input type="text" class="form-control" id="jk" name="jk" required=""> -->
                                            <!-- <select class="form-control" id="jk" name="jk" >
                                                <option value="0">Laki-Laki</option>
                                                <option value="1">Perempuan</option>
                                            </select>
                                            <p id="msg_jk" style="color: red;"></p> -->
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">No. Hp Pemohon</label>
                                            <p><i><b><h5 id="no_hp">Default</h5></b></i></p>
                                            <!-- <input type="number" class="form-control" id="no_hp" name="no_hp" required="">
                                            <p id="msg_no_hp" style="color: red;"></p> -->
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">Tempat, Tanggal Lahir</label>
                                            <p><i><b><h5 id="ttl">Default</h5></b></i></p>
                                            <!-- <input type="text" class="form-control" id="tmp_lhr" name="tmp_lhr" required="">
                                            <p id="msg_tmp_lhr" style="color: red;"></p> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Alamat</label>
                                    <p><i><b><h5 id="alamat">Default</h5></b></i></p>
                                    <!-- <input type="text" class="form-control" id="alamat" name="alamat" required="">
                                    <p id="msg_alamat" style="color: red;"></p> -->
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Pekerjaan</label>
                                    <p><i><b><h5 id="pekerjaan">Default</h5></b></i></p>
                                    <!-- <input type="text" class="form-control" id="pekerjaan" name="pekerjaan" required="">
                                    <p id="msg_pekerjaan" style="color: red;"></p> -->
                                </div>
                            </div>
                            
                        <!-- Tujuan_Permohonan -->
                            <div class="col-md-12">
                                <br><br>
                                <h5><b>Tujuan Permohonan</b></h5>
                                <hr>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Bertempat Dari</label>
                                    <input type="text" class="form-control" id="bertempat_dari" name="bertempat_dari" required="">
                                    <p id="msg_bertempat_dari" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Permintaan Dari</label>
                                    <input type="text" class="form-control" id="permintaan_dari" name="permintaan_dari" required="">
                                    <p id="msg_permintaan_dari" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">keperluan</label>
                                    <input type="text" class="form-control" id="keperluan" name="keperluan" required="">
                                    <p id="msg_keperluan" style="color: red;"></p>
                                </div>
                            </div>

                            <!-- <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Nomer Surat</label>
                                    <input type="text" class="form-control" id="nomer_surat" name="nomer_surat" required="">
                                    <p id="msg_nomer_surat" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Status</label>
                                    <input type="text" class="form-control" id="status" name="status" required="">
                                    <p id="msg_status" style="color: red;"></p>
                                </div>
                            </div> -->
                        <!-- data_awal -->

                        <!-- hasil_wawancara -->
                            <div class="col-md-12">
                                <br>
                                <h5><b>Hasil Wawancara dan Pemeriksaan Fisik</b></h5>
                                <hr>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Kesadaran</label>
                                    <select class="form-control" id="kesadaran" name="kesadaran" required="">
                                        <option value="baik">Baik</option>
                                        <option value="terganggu">Terganggu</option>
                                    </select>
                                    <p id="msg_kesadaran" style="color: red;"></p>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">keumum</label>
                                    <!-- <input type="text" class="form-control" id="keumum" name="keumum" required=""> -->
                                    <select class="form-control" id="keumum" name="keumum" required="">
                                        <option value="baik">Baik</option>
                                        <option value="cukup">Cukup</option>
                                        <option value="kurang">Kurang</option>
                                    </select>
                                    <p id="msg_keumum" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">tekanandarah</label>
                                    <input type="text" class="form-control" id="tekanandarah" name="tekanandarah" required="">
                                    <p id="msg_tekanandarah" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">nadi</label>
                                    <input type="text" class="form-control" id="nadi" name="nadi" required="">
                                    <p id="msg_nadi" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">pernafasan</label>
                                    <input type="text" class="form-control" id="pernafasan" name="pernafasan" required="">
                                    <p id="msg_pernafasan" style="color: red;"></p>
                                </div>
                            </div>
                        <!-- hasil_wawancara -->

                        <!-- riwayat_penggunaan_obat -->
                            <div class="col-md-12">
                                <br>
                                <h5><b>Riwayat Penggunaan Obat-obatan Dalam Seminggu Terakhir</b></h5>
                                <hr>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">penggunaanobat</label>
                                    <select class="form-control" id="penggunaanobat" name="penggunaanobat" required="">
                                        <option value="ada">Ada</option>
                                        <option value="tidak_ada">Tidak Ada</option>
                                    </select>
                                    <p id="msg_penggunaanobat" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">jenisobat</label>
                                    <input type="text" class="form-control" id="jenisobat" name="jenisobat" required="">
                                    <p id="msg_jenisobat" style="color: red;"></p>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">asalobat</label>
                                    <!-- <input type="text" class="form-control" id="asalobat" name="asalobat" required=""> -->
                                    <select class="form-control" id="asalobat" name="asalobat" required="">
                                        <option value="resep dokter">Resep Dokter</option>
                                        <option value="pembelian bebas">Pembelian Bebas</option>
                                        <option value="pemberian">Pemberian</option>
                                        <option value="-">-</option>
                                    </select>
                                    <p id="msg_asalobat" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">terakirminum</label>
                                    <input type="text" class="form-control" id="terakirminum" name="terakirminum" required="">
                                    <p id="msg_terakirminum" style="color: red;"></p>
                                </div>
                            </div>
                        <!-- riwayat_penggunaan_obat -->

                        <!-- tes_urin -->

                            <div class="col-md-12">
                                <br>
                                <h5><b>Hasil Tes Urine</b></h5>
                                <hr>
                            </div>

                            <div class="col-md-6" hidden="">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">hasiltes</label>
                                    <select class="form-control" id="hasiltes" name="hasiltes" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select>
                                    <p id="msg_hasiltes" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">metode</label>
                                    <!-- <select class="form-control" id="metode" name="metode" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select> -->
                                    <input type="text" class="form-control" id="metode" name="metode" required="">
                                    <p id="msg_metode" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">amphe</label>
                                    <!-- <input type="text" class="form-control" id="amphe" name="amphe" required=""> -->
                                    <select class="form-control" id="amphe" name="amphe" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select>
                                    <p id="msg_amphe" style="color: red;"></p>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">metha</label>
                                    <!-- <input type="date" class="form-control" id="metha" name="   metha" required=""> -->
                                    <select class="form-control" id="metha" name="metha" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select>
                                    <p id="msg_metha" style="color: red;"></p>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">coco</label>
                                    <!-- <input type="text" class="form-control" id="coco" name="coco" required=""> -->
                                    <select class="form-control" id="coco" name="coco" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select>
                                    <p id="msg_coco" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">opioid</label>
                                    <!-- <input type="text" class="form-control" id="opioid" name="opioid" required=""> -->
                                    <select class="form-control" id="opioid" name="opioid" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select>
                                    <p id="msg_opioid" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">thc</label>
                                    <!-- <input type="text" class="form-control" id="thc" name="thc" required=""> -->
                                    <select class="form-control" id="thc" name="thc" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select>
                                    <p id="msg_thc" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">benzo</label>
                                    <!-- <input type="text" class="form-control" id="benzo" name="benzo" required=""> -->
                                    <select class="form-control" id="benzo" name="benzo" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select>
                                    <p id="msg_benzo" style="color: red;"></p>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">k2</label>
                                    <!-- <input type="text" class="form-control" id="k2" name="k2" required=""> -->
                                    <select class="form-control" id="k2" name="k2" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select>
                                    <p id="msg_k2" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">lain</label>
                                    <input type="text" class="form-control" id="lain" name="lain" required="">
                                    <p id="msg_lain" style="color: red;"></p>
                                </div>
                            </div>
                        <!-- tes_urin -->

                        <!-- diperiksa oleh -->
                            <div class="col-md-12">
                                <br>
                                <h5><b>Petugas Pemeriksa</b></h5>
                                <hr>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <!-- <label for="message-text" class="control-label">Diperiksa Oleh</label> -->
                                    <select class="form-control" id="pemeriksa" name="pemeriksa" required="">
                                        <?php
                                            if($dokter){
                                                foreach ($dokter as $key => $value) {
                                                    print_r("<option value=\"".$value->id_pejabat."\">".$value->nama_pejabat."</option>");
                                                }
                                            }
                                        ?>
                                    </select>
                                    <p id="msg_pemeriksa" style="color: red;"></p>
                                </div>
                            </div>
                        <!-- diperiksa oleh -->
                            
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <button type="submit" id="add_pasien" class="btn btn-success">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- <div class="card-footer">
                    <div class="col-md-12 text-right">
                        <div class="form-group">
                            <button type="submit" id="add_pasien" class="btn btn-success">Simpan</button>
                        </div>
                    </div>
                </div> -->
                
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->

    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Tabel Data Pemeriksaan</h4>

                </div>
                <div class="card-body">
                    <div class="card">
                        <div class="card-body">

                            <div class="table-responsive m-t-40">
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="5%">No. </th>
                                            <th width="10%">Tgl. Pendaftaran</th>
                                            <th width="20%">Nama Pemohon</th>
                                            <th width="15%">Identitas</th>
                                            <th width="25%">Alamat</th>
                                            <th width="15%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            if(!empty($data_list)){
                                                foreach ($data_list as $key => $value) {
                                                    // print_r("<pre>");
                                                    // print_r($value);

                                                    $str_identitas = "";

                                                    $str_j_iden = "KTP";
                                                    if($value->jenis_identitas == "1"){
                                                        $str_j_iden = "SIM";
                                                    }elseif ($value->jenis_identitas == "2") {
                                                        $str_j_iden = "KTM";
                                                    }

                                                    $str_identitas = "(".$str_j_iden.") ".$value->no_identitas;


                                                    $str_alamat = $value->alamat.", ".$value->kelurahan.", ".$value->kec.", ".$value->kab;


                                                    $str_btn = "<button class=\"btn btn-info\" id=\"up_data\" onclick=\"update_data('".$value->id."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;
                                                                    <button class=\"btn btn-danger\" id=\"del_data\" onclick=\"delete_data('".$value->id."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>&nbsp;&nbsp;
                                                                    <button class=\"btn btn-success\" id=\"vert_data\" onclick=\"vert_data('".$value->id."')\" style=\"width: 40px;\"><i class=\"fa fa-check-square-o\"></i></button>";

                                                    if($value->status == "1"){
                                                        $str_btn = "<button class=\"btn btn-primary\" id=\"srt_data\" onclick=\"srt_data('".$value->id."')\" style=\"width: 40px;\"><i class=\"fa fa-file-text\"></i></button>";
                                                    }


                                                    print_r("<tr>
                                                                <td>".($key+1)."</td>
                                                                <td>".$value->tgl_daftar."</td>
                                                                <td>".$value->nama."</td>
                                                                <td>".$str_identitas."</td>
                                                                <td>".$str_alamat."</td>
                                                                <td>".$str_btn."
                                                                </td>
                                                            </tr>");
                                                }
                                            }     
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="text-right">
                        <label class="form-label">Keterangan Tombol Aksi ==> </label>

                        <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                        <label class="form-label text-info">Update Data</label>,&nbsp;
                        <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                        <label class="form-label text-danger">Delete Data</label>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------update_admin------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="update_data" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Ubah Data Pemeriksaan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <!-- <form action="<?= base_url()."admin_super/superadmin/update_admin";?>" method="post"> -->
            <div class="modal-body">
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="row">
                        <!-- data_awal -->
                            <div class="col-md-12">
                                <h5><b>Data Identitas Terperiksa</b></h5>
                                <hr>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Pasien</label>
                                    <!-- <input type="text" class="form-control" id="id_pasien" name="id_pasien" required=""> -->
                                    <p><i><b><h5 id="_id_pasien">Default</h5></b></i></p>
                                    
                                    <p id="msg_id_pasien" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Foto</label>
                                    <!-- <input type="file" class="form-control" id="foto" name="foto" required="">
                                    <p id="msg_foto" style="color: red;"></p><br> -->
                                    <center>
                                        <img src="" id="_img_foto" style="width:162px ;height:237px ;">
                                    </center>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="row">
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">No. Identitas</label>
                                            <p><i><b><h5 id="_nik">Default</h5></b></i></p>
                                            <!-- <input type="number" class="form-control" id="no_identitas" name="no_identitas" required="">
                                            <p id="msg_no_identitas" style="color: red;"></p> -->
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">Nama Pemohon</label>
                                            <p><i><b><h5 id="_nama">Default</h5></b></i></p>
                                            <!-- <input type="text" class="form-control" id="nama" name="nama" required="">
                                            <p id="msg_nama" style="color: red;"></p> -->
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">Jenis Kelamin</label>
                                            <p><i><b><h5 id="_jk">Default</h5></b></i></p>
                                            <!-- <input type="text" class="form-control" id="jk" name="jk" required=""> -->
                                            <!-- <select class="form-control" id="jk" name="jk" >
                                                <option value="0">Laki-Laki</option>
                                                <option value="1">Perempuan</option>
                                            </select>
                                            <p id="msg_jk" style="color: red;"></p> -->
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">No. Hp Pemohon</label>
                                            <p><i><b><h5 id="_no_hp">Default</h5></b></i></p>
                                            <!-- <input type="number" class="form-control" id="no_hp" name="no_hp" required="">
                                            <p id="msg_no_hp" style="color: red;"></p> -->
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">Tempat, Tanggal Lahir</label>
                                            <p><i><b><h5 id="_ttl">Default</h5></b></i></p>
                                            <!-- <input type="text" class="form-control" id="tmp_lhr" name="tmp_lhr" required="">
                                            <p id="msg_tmp_lhr" style="color: red;"></p> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                            
                            
                            
                            
                            



                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Alamat</label>
                                    <p><i><b><h5 id="_alamat">Default</h5></b></i></p>
                                    <!-- <input type="text" class="form-control" id="alamat" name="alamat" required="">
                                    <p id="msg_alamat" style="color: red;"></p> -->
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Pekerjaan</label>
                                    <p><i><b><h5 id="_pekerjaan">Default</h5></b></i></p>
                                    <!-- <input type="text" class="form-control" id="pekerjaan" name="pekerjaan" required="">
                                    <p id="msg_pekerjaan" style="color: red;"></p> -->
                                </div>
                            </div>
                            
                        <!-- Tujuan_Permohonan -->
                            <div class="col-md-12">
                                <br><br>
                                <h5><b>Tujuan Permohonan</b></h5>
                                <hr>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Bertempat Dari</label>
                                    <input type="text" class="form-control" id="_bertempat_dari" name="bertempat_dari" required="">
                                    <p id="_msg_bertempat_dari" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Permintaan Dari</label>
                                    <input type="text" class="form-control" id="_permintaan_dari" name="permintaan_dari" required="">
                                    <p id="_msg_permintaan_dari" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">keperluan</label>
                                    <input type="text" class="form-control" id="_keperluan" name="keperluan" required="">
                                    <p id="_msg_keperluan" style="color: red;"></p>
                                </div>
                            </div>

                            <!-- <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Nomer Surat</label>
                                    <input type="text" class="form-control" id="nomer_surat" name="nomer_surat" required="">
                                    <p id="msg_nomer_surat" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Status</label>
                                    <input type="text" class="form-control" id="status" name="status" required="">
                                    <p id="msg_status" style="color: red;"></p>
                                </div>
                            </div> -->
                        <!-- data_awal -->

                        <!-- hasil_wawancara -->
                            <div class="col-md-12">
                                <br>
                                <h5><b>Hasil Wawancara dan Pemeriksaan Fisik</b></h5>
                                <hr>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Kesadaran</label>
                                    <select class="form-control" id="_kesadaran" name="kesadaran" required="">
                                        <option value="baik">Baik</option>
                                        <option value="terganggu">Terganggu</option>
                                    </select>
                                    <p id="_msg_kesadaran" style="color: red;"></p>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">keumum</label>
                                    <!-- <input type="text" class="form-control" id="keumum" name="keumum" required=""> -->
                                    <select class="form-control" id="_keumum" name="keumum" required="">
                                        <option value="baik">Baik</option>
                                        <option value="cukup">Cukup</option>
                                        <option value="kurang">Kurang</option>
                                    </select>
                                    <p id="_msg_keumum" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">tekanandarah</label>
                                    <input type="text" class="form-control" id="_tekanandarah" name="tekanandarah" required="">
                                    <p id="_msg_tekanandarah" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">nadi</label>
                                    <input type="text" class="form-control" id="_nadi" name="nadi" required="">
                                    <p id="_msg_nadi" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">pernafasan</label>
                                    <input type="text" class="form-control" id="_pernafasan" name="pernafasan" required="">
                                    <p id="_msg_pernafasan" style="color: red;"></p>
                                </div>
                            </div>
                        <!-- hasil_wawancara -->

                        <!-- riwayat_penggunaan_obat -->
                            <div class="col-md-12">
                                <br>
                                <h5><b>Riwayat Penggunaan Obat-obatan Dalam Seminggu Terakhir</b></h5>
                                <hr>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">penggunaanobat</label>
                                    <select class="form-control" id="_penggunaanobat" name="penggunaanobat" required="">
                                        <option value="ada">Ada</option>
                                        <option value="tidak_ada">Tidak Ada</option>
                                    </select>
                                    <p id="_msg_penggunaanobat" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">jenisobat</label>
                                    <input type="text" class="form-control" id="_jenisobat" name="jenisobat" required="">
                                    <p id="_msg_jenisobat" style="color: red;"></p>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">asalobat</label>
                                    <!-- <input type="text" class="form-control" id="asalobat" name="asalobat" required=""> -->
                                    <select class="form-control" id="_asalobat" name="asalobat" required="">
                                        <option value="resep dokter">Resep Dokter</option>
                                        <option value="pembelian bebas">Pembelian Bebas</option>
                                        <option value="pemberian">Pemberian</option>
                                        <option value="-">-</option>
                                    </select>
                                    <p id="_msg_asalobat" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">terakirminum</label>
                                    <input type="text" class="form-control" id="_terakirminum" name="terakirminum" required="">
                                    <p id="_msg_terakirminum" style="color: red;"></p>
                                </div>
                            </div>
                        <!-- riwayat_penggunaan_obat -->

                        <!-- tes_urin -->

                            <div class="col-md-12">
                                <br>
                                <h5><b>Hasil Tes Urine</b></h5>
                                <hr>
                            </div>

                            <div class="col-md-6" hidden="">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">hasiltes</label>
                                    <select class="form-control" id="_hasiltes" name="hasiltes" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select>
                                    <p id="_msg_hasiltes" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">metode</label>
                                    <!-- <select class="form-control" id="_metode" name="metode" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select> -->
                                    <input type="text" class="form-control" id="_metode" name="metode" required="">
                                    <p id="_msg_metode" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">amphe</label>
                                    <!-- <input type="text" class="form-control" id="amphe" name="amphe" required=""> -->
                                    <select class="form-control" id="_amphe" name="amphe" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select>
                                    <p id="_msg_amphe" style="color: red;"></p>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">metha</label>
                                    <!-- <input type="date" class="form-control" id="metha" name="   metha" required=""> -->
                                    <select class="form-control" id="_metha" name="metha" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select>
                                    <p id="_msg_metha" style="color: red;"></p>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">coco</label>
                                    <!-- <input type="text" class="form-control" id="coco" name="coco" required=""> -->
                                    <select class="form-control" id="_coco" name="coco" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select>
                                    <p id="_msg_coco" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">opioid</label>
                                    <!-- <input type="text" class="form-control" id="opioid" name="opioid" required=""> -->
                                    <select class="form-control" id="_opioid" name="opioid" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select>
                                    <p id="_msg_opioid" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">thc</label>
                                    <!-- <input type="text" class="form-control" id="thc" name="thc" required=""> -->
                                    <select class="form-control" id="_thc" name="thc" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select>
                                    <p id="_msg_thc" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">benzo</label>
                                    <!-- <input type="text" class="form-control" id="benzo" name="benzo" required=""> -->
                                    <select class="form-control" id="_benzo" name="benzo" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select>
                                    <p id="_msg_benzo" style="color: red;"></p>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">k2</label>
                                    <!-- <input type="text" class="form-control" id="k2" name="k2" required=""> -->
                                    <select class="form-control" id="_k2" name="k2" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select>
                                    <p id="_msg_k2" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Lain-Lain</label>
                                    <input type="text" class="form-control" id="_lain" name="lain" required="">
                                    <p id="_msg_lain" style="color: red;"></p>
                                </div>
                            </div>
                        <!-- tes_urin -->
                        <!-- diperiksa oleh -->
                            <div class="col-md-12">
                                <br>
                                <h5><b>Petugas Pemeriksa</b></h5>
                                <hr>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <!-- <label for="message-text" class="control-label">Diperiksa Oleh</label> -->
                                    <select class="form-control" id="_pemeriksa" name="pemeriksa" required="">
                                        <?php
                                            if($dokter){
                                                foreach ($dokter as $key => $value) {
                                                    print_r("<option value=\"".$value->id_pejabat."\">".$value->nama_pejabat."</option>");
                                                }
                                            }
                                        ?>
                                    </select>
                                    <p id="_msg_pemeriksa" style="color: red;"></p>
                                </div>
                            </div>
                        <!-- diperiksa oleh -->
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="submit" id="btn_update_data" class="btn btn-success">Ubah Data</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------update_admin------------------------ -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------vert_data------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="vert_data_modal" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Verifikasi Data Pemeriksaan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <!-- <form action="<?= base_url()."admin_super/superadmin/vert_data";?>" method="post"> -->
            <div class="modal-body">
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="row">
                        <!-- data_awal -->
                            <div class="col-md-12">
                                <h5><b>Data Identitas Terperiksa</b></h5>
                                <hr>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Pasien</label>
                                    <!-- <input type="text" class="form-control" id="id_pasien" name="id_pasien" required=""> -->
                                    <p><i><b><h5 id="vert_id_pasien">Default</h5></b></i></p>
                                    
                                    <p id="msg_id_pasien" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Foto</label>
                                    <!-- <input type="file" class="form-control" id="foto" name="foto" required="">
                                    <p id="msg_foto" style="color: red;"></p><br> -->
                                    <center>
                                        <img src="" id="vert_img_foto" style="width:162px ;height:237px ;">
                                    </center>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="row">
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">No. Identitas</label>
                                            <p><i><b><h5 id="vert_nik">Default</h5></b></i></p>
                                            <!-- <input type="number" class="form-control" id="no_identitas" name="no_identitas" required="">
                                            <p id="msg_no_identitas" style="color: red;"></p> -->
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">Nama Pemohon</label>
                                            <p><i><b><h5 id="vert_nama">Default</h5></b></i></p>
                                            <!-- <input type="text" class="form-control" id="nama" name="nama" required="">
                                            <p id="msg_nama" style="color: red;"></p> -->
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">Jenis Kelamin</label>
                                            <p><i><b><h5 id="vert_jk">Default</h5></b></i></p>
                                            <!-- <input type="text" class="form-control" id="jk" name="jk" required=""> -->
                                            <!-- <select class="form-control" id="jk" name="jk" >
                                                <option value="0">Laki-Laki</option>
                                                <option value="1">Perempuan</option>
                                            </select>
                                            <p id="msg_jk" style="color: red;"></p> -->
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">No. Hp Pemohon</label>
                                            <p><i><b><h5 id="vert_no_hp">Default</h5></b></i></p>
                                            <!-- <input type="number" class="form-control" id="no_hp" name="no_hp" required="">
                                            <p id="msg_no_hp" style="color: red;"></p> -->
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">Tempat, Tanggal Lahir</label>
                                            <p><i><b><h5 id="vert_ttl">Default</h5></b></i></p>
                                            <!-- <input type="text" class="form-control" id="tmp_lhr" name="tmp_lhr" required="">
                                            <p id="msg_tmp_lhr" style="color: red;"></p> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                            
                            
                            
                            
                            



                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Alamat</label>
                                    <p><i><b><h5 id="vert_alamat">Default</h5></b></i></p>
                                    <!-- <input type="text" class="form-control" id="alamat" name="alamat" required="">
                                    <p id="msg_alamat" style="color: red;"></p> -->
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Pekerjaan</label>
                                    <p><i><b><h5 id="vert_pekerjaan">Default</h5></b></i></p>
                                    <!-- <input type="text" class="form-control" id="pekerjaan" name="pekerjaan" required="">
                                    <p id="msg_pekerjaan" style="color: red;"></p> -->
                                </div>
                            </div>
                            
                        <!-- Tujuan_Permohonan -->
                            <div class="col-md-12">
                                <br><br>
                                <h5><b>Tujuan Permohonan</b></h5>
                                <hr>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Bertempat Dari</label>
                                    <!-- <input type="text" class="form-control" id="_bertempat_dari" name="bertempat_dari" required=""> -->
                                    <p><i><b><h5 id="vert_bertempat_dari">Default</h5></b></i></p>
                                    <!-- <p id="_msg_bertempat_dari" style="color: red;"></p> -->
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Permintaan Dari</label>
                                    <!-- <input type="text" class="form-control" id="_permintaan_dari" name="permintaan_dari" required=""> -->
                                    <p><i><b><h5 id="vert_permintaan_dari">Default</h5></b></i></p>
                                    <!-- <p id="_msg_permintaan_dari" style="color: red;"></p> -->
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">keperluan</label>
                                    <!-- <input type="text" class="form-control" id="_keperluan" name="keperluan" required=""> -->
                                    <p><i><b><h5 id="vert_keperluan">Default</h5></b></i></p>
                                    <!-- <p id="_msg_keperluan" style="color: red;"></p> -->
                                </div>
                            </div>

                        <!-- data_awal -->

                        <!-- hasil_wawancara -->
                            <div class="col-md-12">
                                <br>
                                <h5><b>Hasil Wawancara dan Pemeriksaan Fisik</b></h5>
                                <hr>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Kesadaran</label>
                                    <!-- <select class="form-control" id="_kesadaran" name="kesadaran" required="">
                                        <option value="baik">Baik</option>
                                        <option value="terganggu">Terganggu</option>
                                    </select> -->
                                    <p><i><b><h5 id="vert_kesadaran">Default</h5></b></i></p>
                                    <!-- <p id="_msg_kesadaran" style="color: red;"></p> -->
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">keumum</label>
                                    <!-- <input type="text" class="form-control" id="keumum" name="keumum" required=""> -->
                                    <!-- <select class="form-control" id="_keumum" name="keumum" required="">
                                        <option value="baik">Baik</option>
                                        <option value="cukup">Cukup</option>
                                        <option value="kurang">Kurang</option>
                                    </select> -->
                                    <p><i><b><h5 id="vert_keumum">Default</h5></b></i></p>
                                    <!-- <p id="_msg_keumum" style="color: red;"></p> -->
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">tekanandarah</label>
                                    <!-- <input type="text" class="form-control" id="_tekanandarah" name="tekanandarah" required=""> -->
                                    <p><i><b><h5 id="vert_tekanandarah">Default</h5></b></i></p>
                                    <!-- <p id="_msg_tekanandarah" style="color: red;"></p> -->
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">nadi</label>
                                    <!-- <input type="text" class="form-control" id="_nadi" name="nadi" required=""> -->
                                    <p><i><b><h5 id="vert_nadi">Default</h5></b></i></p>
                                    <!-- <p id="_msg_nadi" style="color: red;"></p> -->
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">pernafasan</label>
                                    <!-- <input type="text" class="form-control" id="_pernafasan" name="pernafasan" required=""> -->
                                    <p><i><b><h5 id="vert_pernafasan">Default</h5></b></i></p>
                                    <!-- <p id="_msg_pernafasan" style="color: red;"></p> -->
                                </div>
                            </div>
                        <!-- hasil_wawancara -->

                        <!-- riwayat_penggunaan_obat -->
                            <div class="col-md-12">
                                <br>
                                <h5><b>Riwayat Penggunaan Obat-obatan Dalam Seminggu Terakhir</b></h5>
                                <hr>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">penggunaanobat</label>
                                    <!-- <select class="form-control" id="_penggunaanobat" name="penggunaanobat" required="">
                                        <option value="ada">Ada</option>
                                        <option value="tidak_ada">Tidak Ada</option>
                                    </select> -->
                                    <p><i><b><h5 id="vert_penggunaanobat">Default</h5></b></i></p>
                                    <p id="_msg_penggunaanobat" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">jenisobat</label>
                                    <!-- <input type="text" class="form-control" id="_jenisobat" name="jenisobat" required=""> -->
                                    <p><i><b><h5 id="vert_jenisobat">Default</h5></b></i></p>
                                    <!-- <p id="_msg_jenisobat" style="color: red;"></p> -->
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">asalobat</label>
                                    <!-- <input type="text" class="form-control" id="asalobat" name="asalobat" required=""> -->
                                    <!-- <select class="form-control" id="_asalobat" name="asalobat" required="">
                                        <option value="resep dokter">Resep Dokter</option>
                                        <option value="pembelian bebas">Pembelian Bebas</option>
                                        <option value="pemberian">Pemberian</option>
                                    </select> -->
                                    <p><i><b><h5 id="vert_asalobat">Default</h5></b></i></p>
                                    <!-- <p id="_msg_asalobat" style="color: red;"></p> -->
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">terakirminum</label>
                                    <!-- <input type="text" class="form-control" id="_terakirminum" name="terakirminum" required=""> -->
                                    <p><i><b><h5 id="vert_terakirminum">Default</h5></b></i></p>
                                    <!-- <p id="_msg_terakirminum" style="color: red;"></p> -->
                                </div>
                            </div>
                        <!-- riwayat_penggunaan_obat -->

                        <!-- tes_urin -->

                            <div class="col-md-12">
                                <br>
                                <h5><b>Hasil Tes Urine</b></h5>
                                <hr>
                            </div>

                            <div class="col-md-6" hidden="">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">hasiltes</label>
                                    <!-- <select class="form-control" id="_hasiltes" name="hasiltes" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select> -->
                                    <p><i><b><h5 id="vert_hasiltes">Default</h5></b></i></p>
                                    <!-- <p id="_msg_hasiltes" style="color: red;"></p> -->
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">metode</label>
                                    <!-- <select class="form-control" id="_metode" name="metode" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select> -->
                                    <p><i><b><h5 id="vert_metode">Default</h5></b></i></p>
                                    <!-- <p id="_msg_metode" style="color: red;"></p> -->
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">amphe</label>
                                    <!-- <input type="text" class="form-control" id="amphe" name="amphe" required=""> -->
                                    <!-- <select class="form-control" id="_amphe" name="amphe" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select> -->
                                    <p><i><b><h5 id="vert_amphe">Default</h5></b></i></p>
                                    <!-- <p id="_msg_amphe" style="color: red;"></p> -->
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">metha</label>
                                    <!-- <input type="date" class="form-control" id="metha" name="   metha" required=""> -->
                                    <!-- <select class="form-control" id="_metha" name="metha" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select> -->
                                    <p><i><b><h5 id="vert_metha">Default</h5></b></i></p>
                                    <!-- <p id="_msg_metha" style="color: red;"></p> -->
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">coco</label>
                                    <!-- <input type="text" class="form-control" id="coco" name="coco" required=""> -->
                                    <!-- <select class="form-control" id="_coco" name="coco" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select> -->
                                    <p><i><b><h5 id="vert_coco">Default</h5></b></i></p>
                                    <!-- <p id="_msg_coco" style="color: red;"></p> -->
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">opioid</label>
                                    <!-- <input type="text" class="form-control" id="opioid" name="opioid" required=""> -->
                                    <!-- <select class="form-control" id="_opioid" name="opioid" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select> -->
                                    <p><i><b><h5 id="vert_opioid">Default</h5></b></i></p>
                                    <!-- <p id="_msg_opioid" style="color: red;"></p> -->
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">thc</label>
                                    <!-- <input type="text" class="form-control" id="thc" name="thc" required=""> -->
                                    <!-- <select class="form-control" id="_thc" name="thc" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select> -->
                                    <p><i><b><h5 id="vert_thc">Default</h5></b></i></p>
                                    <!-- <p id="_msg_thc" style="color: red;"></p> -->
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">benzo</label>
                                    <!-- <input type="text" class="form-control" id="benzo" name="benzo" required=""> -->
                                    <!-- <select class="form-control" id="_benzo" name="benzo" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select> -->
                                    <p><i><b><h5 id="vert_benzo">Default</h5></b></i></p>
                                    <!-- <p id="_msg_benzo" style="color: red;"></p> -->
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">k2</label>
                                    <!-- <input type="text" class="form-control" id="k2" name="k2" required=""> -->
                                    <!-- <select class="form-control" id="_k2" name="k2" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select> -->
                                    <p><i><b><h5 id="vert_k2">Default</h5></b></i></p>
                                    <!-- <p id="_msg_k2" style="color: red;"></p> -->
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">lain</label>
                                    <!-- <input type="text" class="form-control" id="_lain" name="lain" required=""> -->
                                    <p><i><b><h5 id="vert_lain">Default</h5></b></i></p>
                                    <!-- <p id="_msg_lain" style="color: red;"></p> -->
                                </div>
                            </div>
                        <!-- tes_urin -->

                        <!-- Pemeriksa -->
                            <div class="col-md-12">
                                <br>
                                <h5><b>Diperiksa Oleh</b></h5>
                                <hr>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <!-- <label for="message-text" class="control-label">hasiltes</label> -->
                                    <!-- <select class="form-control" id="_hasiltes" name="hasiltes" required="">
                                        <option value="positif">positif</option>
                                        <option value="negatif">negatif</option>
                                    </select> -->
                                    <p><i><b><h5 id="vert_pemeriksa">Default</h5></b></i></p>
                                    <!-- <p id="_msg_hasiltes" style="color: red;"></p> -->
                                </div>
                            </div>
                        <!-- Pemeriksa -->
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="submit" id="btn_vert_data_data" class="btn btn-success">Verifikasi Data</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------vert_data------------------------ -->
<!-- ============================================================== -->

<script type="text/javascript">
    var id_admin_glob = "";
    var key_ex, id_admin_op;

        $(document).ready(function(){
            penggunaanobat_ch();
            _penggunaanobat_ch();
            var val =$("#id_pasien").val();
            // console.log(val);

            var data_main = new FormData();
            data_main.append('id_data',"");

            $.ajax({
                url: "<?php echo base_url()."pemeriksaan/get_data_iden/";?>"+val,
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    get_iden(res);
                }
            });
        });

        $("#id_pasien").change(function(){
            var val =$("#id_pasien").val();
            // console.log(val);

            var data_main = new FormData();
            data_main.append('id_data',"");

            $.ajax({
                url: "<?php echo base_url()."pemeriksaan/get_data_iden/";?>"+val,
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    get_iden(res);
                }
            });
        });

        function get_iden(res){
            var data = JSON.parse(res);
            console.log(data);
            // var data_main =
            var str_nik = data.msg_detail.item.no_identitas;
            if(data.msg_detail.item.jenis_identitas == "0"){
                str_nik += " (KTP)";
            }else if(data.msg_detail.item.jenis_identitas == "1"){
                str_nik += " (SIM)";
            }else{
                str_nik += " (Kartiu Tanda Mahasiswa)";
            }

            var str_jk = "Laki-Laki";
            if(data.msg_detail.item.jk == "1"){
                str_jk = "Perempuan";
            }

            if(data.msg_main.status){
                $("#img_foto").prop("src","<?php print_r(base_url()."assets/doc/pasien/");?>"+data.msg_detail.item.foto);
                $("#nik").html(str_nik);
                $("#nama").html(data.msg_detail.item.nama);
                $("#jk").html(str_jk);
                $("#no_hp").html(data.msg_detail.item.no_hp);
                $("#ttl").html(data.msg_detail.item.tmp_lhr+", "+data.msg_detail.item.tgl_lhr);
                $("#alamat").html(data.msg_detail.item.alamat+", "+data.msg_detail.item.kelurahan+", "+data.msg_detail.item.kecamatan+", "+data.msg_detail.item.kab);
                $("#pekerjaan").html(data.msg_detail.item.pekerjaan);
            }else{
                $("#img_foto").prop("src","");
                $("#nik").html("");
                $("#nama").html("");
                $("#jk").html("");
                $("#no_hp").html("");
                $("#ttl").html("");
                $("#alamat").html("");
                $("#pekerjaan").html("");
            }
        }

    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//


        $("#add_pasien").click(function() {
            var data_main = new FormData();
            data_main.append('id_pasien', $("#id_pasien").val());
            data_main.append('bertempat_dari', $("#bertempat_dari").val());
            data_main.append('permintaan_dari', $("#permintaan_dari").val());
            data_main.append('kesadaran', $("#kesadaran").val());
            data_main.append('keumum', $("#keumum").val());
            data_main.append('tekanandarah', $("#tekanandarah").val());
            data_main.append('nadi', $("#nadi").val());
            data_main.append('pernafasan', $("#pernafasan").val());
            data_main.append('penggunaanobat', $("#penggunaanobat").val());
            data_main.append('jenisobat', $("#jenisobat").val());
            data_main.append('asalobat', $("#asalobat").val());
            data_main.append('terakirminum', $("#terakirminum").val());
            data_main.append('hasiltes', $("#hasiltes").val());

            data_main.append('metode', $("#metode").val());
            data_main.append('amphe', $("#amphe").val());
            data_main.append('metha', $("#metha").val());
            data_main.append('coco', $("#coco").val());
            data_main.append('opioid', $("#opioid").val());
            data_main.append('thc', $("#thc").val());
            data_main.append('benzo', $("#benzo").val());
            data_main.append('k2', $("#k2").val());
            data_main.append('lain', $("#lain").val());
            data_main.append('keperluan', $("#keperluan").val());

            data_main.append('pemeriksa', $("#pemeriksa").val());
           

            $.ajax({
                url: "<?php echo base_url()."Pemeriksaan/insert_pemeriksaan";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_insert(res);
                }
            });
        });

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            swal({
                                title: "Proses Berhasil.!!",
                                text: "Data Pemeriksaan berhasil disimpan ..!",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#28a745",
                                confirmButtonText: "Lanjutkan",
                                closeOnConfirm: false
                            }, function() {
                                window.location.href = "<?php echo base_url()."admin/data/pemeriksaan";?>";
                            });
                        },
                        //init
                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            } else {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            // $("#msg_id").html(detail_msg.no_surat);
                            // $("#msg_jenis_identitas").html(detail_msg.jenis_identitas);
                            // $("#msg_no_identitas").html(detail_msg.no_identitas);
                            // $("#msg_foto").html(detail_msg.foto);
                            // $("#msg_no_hp").html(detail_msg.no_hp);
                            // $("#msg_nama").html(detail_msg.nama);
                            // $("#msg_alamat").html(detail_msg.alamat);
                            // $("#msg_jk").html(detail_msg.jk);
                            // $("#msg_tmp_lhr").html(detail_msg.tmp_lhr);
                            // $("#msg_tgl_lhr").html(detail_msg.tgl_lhr);
                            // $("#msg_kel").html(detail_msg.kel);
                            // $("#msg_kec").html(detail_msg.kec);
                            // $("#msg_kab").html(detail_msg.kab);
                            // $("#msg_pekerjaan").html(detail_msg.pekerjaan);


                            

                            swal("Proses Gagal.!!", "Data Pemeriksaan gagal disimpan, coba periksa jaringan dan koneksi anda", "warning");
                        },

                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);

            }
        }
    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//
        function update_data(id_admin) {
            var id_admin_chahce = "";
            clear_from_update();

            var data_main = new FormData();
            data_main.append('id_data', id_admin);

            $.ajax({
                url: "<?php echo base_url()."pemeriksaan/get_pemeriksaan_update/";?>"+id_admin,
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    set_val_update(res, id_admin);
                    $("#update_data").modal('show');
                }
            });
        }

        function set_val_update(res, id_admin) {
            var res_pemohon = JSON.parse(res);
            console.log(res_pemohon);
            if (res_pemohon.status == true) {
                id_admin_chahce = res_pemohon.val_response.id;
                
                var str_nik = res_pemohon.val_response.no_identitas;
                if(res_pemohon.val_response.jenis_identitas == "0"){
                    str_nik += " (KTP)";
                }else if(res_pemohon.val_response.jenis_identitas == "1"){
                    str_nik += " (SIM)";
                }else{
                    str_nik += " (Kartiu Tanda Mahasiswa)";
                }

                var str_jk = "Laki-Laki";
                if(res_pemohon.val_response.jk == "1"){
                    str_jk = "Perempuan";
                }
                _penggunaanobat_ch();

                if(res_pemohon.status){
                    $("#_id_pasien").html(res_pemohon.val_response.id);
                    $("#_img_foto").prop("src","<?php print_r(base_url()."assets/doc/pasien/");?>"+res_pemohon.val_response.foto);
                    $("#_nik").html(str_nik);
                    $("#_nama").html(res_pemohon.val_response.nama);
                    $("#_jk").html(str_jk);
                    $("#_no_hp").html(res_pemohon.val_response.no_hp);
                    $("#_ttl").html(res_pemohon.val_response.tmp_lhr+", "+res_pemohon.val_response.tgl_lhr);
                    $("#_alamat").html(res_pemohon.val_response.alamat+", "+res_pemohon.val_response.kelurahan+", "+res_pemohon.val_response.kecamatan+", "+res_pemohon.val_response.kab);
                    $("#_pekerjaan").html(res_pemohon.val_response.pekerjaan);

                    $("#_bertempat_dari").val(res_pemohon.val_response.bertempat_dari);
                    $("#_permintaan_dari").val(res_pemohon.val_response.permintaan_dari);
                    $("#_kesadaran").val(res_pemohon.val_response.kesadaran);
                    $("#_keumum").val(res_pemohon.val_response.keumum);
                    $("#_tekanandarah").val(res_pemohon.val_response.tekanandarah);
                    $("#_nadi").val(res_pemohon.val_response.nadi);
                    $("#_pernafasan").val(res_pemohon.val_response.pernafasan);
                    $("#_penggunaanobat").val(res_pemohon.val_response.penggunaanobat);
                    $("#_jenisobat").val(res_pemohon.val_response.jenisobat);
                    $("#_asalobat").val(res_pemohon.val_response.asalobat);
                    $("#_terakirminum").val(res_pemohon.val_response.terakirminum);
                    $("#_hasiltes").val(res_pemohon.val_response.hasiltes);
                    $("#_metode").val(res_pemohon.val_response.metode);
                    $("#_amphe").val(res_pemohon.val_response.amphe);
                    $("#_metha").val(res_pemohon.val_response.metha);
                    $("#_coco").val(res_pemohon.val_response.coco);
                    $("#_opioid").val(res_pemohon.val_response.opioid);
                    $("#_thc").val(res_pemohon.val_response.thc);
                    $("#_benzo").val(res_pemohon.val_response.benzo);
                    $("#_k2").val(res_pemohon.val_response.k2);
                    $("#_lain").val(res_pemohon.val_response.lain);
                    $("#_keperluan").val(res_pemohon.val_response.keperluan);
                    $("#_pemeriksa").val(res_pemohon.val_response.id_pejabat);


                }else{
                    $("#_img_foto").prop("src","");
                    $("#_nik").html("");
                    $("#_nama").html("");
                    $("#_jk").html("");
                    $("#_no_hp").html("");
                    $("#_ttl").html("");
                    $("#_alamat").html("");
                    $("#_pekerjaan").html("");

                    $("#_bertempat_dari").val("");
                    $("#_permintaan_dari").val("");
                    $("#_kesadaran").val("");
                    $("#_keumum").val("");
                    $("#_tekanandarah").val("");
                    $("#_nadi").val("");
                    $("#_pernafasan").val("");
                    $("#_penggunaanobat").val("");
                    $("#_jenisobat").val("");
                    $("#_asalobat").val("");
                    $("#_terakirminum").val("");
                    $("#_hasiltes").val("");
                    $("#_metode").val("");
                    $("#_amphe").val("");
                    $("#_metha").val("");
                    $("#_coco").val("");
                    $("#_metodemetode").val("");
                    $("#_thc").val("");
                    $("#_benzo").val("");
                    $("#_k2").val("");
                    $("#_lain").val("");
                    $("#_keperluan").val("");
                    $("#_pemeriksa").val("");
                }
               
                id_admin_glob = id_admin;
            } else {
                clear_from_update();
            }

        }

        function clear_from_update() {
            $("#_no_surat").val("");
            $("#_jenis_identitas").val("");
            $("#_no_identitas").val("");
            $("#_img_foto").val("");
            $("#_no_hp").val("");
            $("#_nama").val("");
            $("#_alamat").val("");
            $("#_jk").val("");
            $("#_tmp_lhr").val("");
            $("#_tgl_lhr").html("");
            $("#_kel").val("");
            $("#_kec").val("");
            $("#_kab").val("");
            $("#_pekerjaan").val("");

            id_admin_glob = "";
            $("#update_data").modal('hide');
        }
    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//

        var file = [];
        $("#_foto").change(function(e){
            file = e.target.files[0];

            $("#_img_foto").attr("src",URL.createObjectURL(file));
            console.log(file);
        });

        $("#btn_update_data").click(function() {
            var data_main = new FormData();
            data_main.append('id_pasien', $("#_id_pasien").val());
            data_main.append('bertempat_dari', $("#_bertempat_dari").val());
            data_main.append('permintaan_dari', $("#_permintaan_dari").val());
            data_main.append('kesadaran', $("#_kesadaran").val());
            data_main.append('keumum', $("#_keumum").val());
            data_main.append('tekanandarah', $("#_tekanandarah").val());
            data_main.append('nadi', $("#_nadi").val());
            data_main.append('pernafasan', $("#_pernafasan").val());
            data_main.append('penggunaanobat', $("#_penggunaanobat").val());
            data_main.append('jenisobat', $("#_jenisobat").val());
            data_main.append('asalobat', $("#_asalobat").val());
            data_main.append('terakirminum', $("#_terakirminum").val());
            data_main.append('hasiltes', $("#_hasiltes").val());

            data_main.append('metode', $("#_metode").val());
            data_main.append('amphe', $("#_amphe").val());
            data_main.append('metha', $("#_metha").val());
            data_main.append('coco', $("#_coco").val());
            data_main.append('opioid', $("#_opioid").val());
            data_main.append('thc', $("#_thc").val());
            data_main.append('benzo', $("#_benzo").val());
            data_main.append('k2', $("#_k2").val());
            data_main.append('lain', $("#_lain").val());
            data_main.append('keperluan', $("#_keperluan").val());

            data_main.append('pemeriksa', $("#_pemeriksa").val());

            data_main.append('id', id_admin_glob);

            $.ajax({
                url: "<?php echo base_url()."pemeriksaan/update_pemeriksaan";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_update(res);
                }
            });
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            swal({
                                title: "Proses Berhasil.!!",
                                text: "Data Pemeriksaan berhasil diubah ..!",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#28a745",
                                confirmButtonText: "Lanjutkan",
                                closeOnConfirm: false
                            }, function() {
                                window.location.href = "<?php echo base_url()."admin/data/pemeriksaan";?>";
                            });
                        },
                        //init
                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            } else {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            
                        $("#_msg_no_surat").val("");
                        $("#_msg_jenis_identitas").val("");
                        $("#_msg_no_identitas").val("");
                        $("#_msg_foto").val("");
                        $("#_msg_no_hp").val("");
                        $("#_msg_nama").val("");
                        $("#_msg_alamat").val("");
                        $("#_msg_jk").val("");
                        $("#_msg_tmp_lhr").val("");
                        $("#_msg_tgl_lhr").html("");
                        $("#_msg_kel").val("");
                        $("#_msg_kec").val("");
                        $("#_msg_kab").val("");
                        $("#_msg_pekerjaan").val("");
                           
                            
                            swal("Proses Gagal.!!", "Data Pemeriksaan gagal diubah, coba periksa jaringan dan koneksi anda", "warning");
                        },

                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            }
        }
    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------admin_delete--------------------------//
    //=========================================================================//
        function delete_data(id_admin) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({
                            title: "Pesan Konfirmasi",
                            text: "Silahkan Cermati data sebelem di hapus, jika anda sudah yakin maka data ini dan seluruh data yang berkaitan akan di hapus",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#ffb22b",
                            confirmButtonText: "Hapus",
                            closeOnConfirm: false
                        }, function() {
                            var data_main = new FormData();
                            data_main.append('id', id_admin);

                            $.ajax({
                                url: "<?php echo base_url()."pemeriksaan/delete_pemeriksaan/";?>",
                                dataType: 'html', // what to expect back from the PHP script, if anything
                                cache: false,
                                contentType: false,
                                processData: false,
                                data: data_main,
                                type: 'post',
                                success: function(res) {
                                    // console.log(id_admin);
                                    response_delete(res);
                                }
                            });
                        });
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }

        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");

                swal({
                    title: "Proses Berhasil.!!",
                    text: "Data Pemeriksaan berhasil dihapus ..!",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#28a745",
                    confirmButtonText: "Lanjutkan",
                    closeOnConfirm: false
                }, function() {
                    window.location.href = "<?php echo base_url()."admin/data/pemeriksaan";?>";
                });

            } else {
                // console.log("false");
                swal("Proses Gagal.!!", "Data Pemeriksaan gagal dihapus, coba periksa jaringan dan koneksi anda", "warning");
            }
        }
    //=========================================================================//
    //-----------------------------------admin_update--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------vert_data-----------------------------//
    //=========================================================================//
        function vert_data(id_admin) {
            var id_admin_chahce = "";
            clear_from_update();

            console.log(id_admin);

            var data_main = new FormData();
            data_main.append('id_data', id_admin);

            $.ajax({
                url: "<?php echo base_url()."pemeriksaan/get_pemeriksaan_update/";?>"+id_admin,
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    set_val_vert_data(res, id_admin);
                    $("#vert_data_modal").modal('show');
                }
            });
        }

        var id_vert_chahce;
        var id_vert_glob;

        function set_val_vert_data(res, id_admin) {
            var res_pemohon = JSON.parse(res);
            console.log(res_pemohon);
            if (res_pemohon.status == true) {
                id_vert_chahce = res_pemohon.val_response.id;
                
                var str_nik = res_pemohon.val_response.no_identitas;
                if(res_pemohon.val_response.jenis_identitas == "0"){
                    str_nik += " (KTP)";
                }else if(res_pemohon.val_response.jenis_identitas == "1"){
                    str_nik += " (SIM)";
                }else{
                    str_nik += " (Kartiu Tanda Mahasiswa)";
                }

                var str_jk = "Laki-Laki";
                if(res_pemohon.val_response.jk == "1"){
                    str_jk = "Perempuan";
                }

                if(res_pemohon.status){
                    $("#vert_id_pasien").html(res_pemohon.val_response.id);
                    $("#vert_img_foto").prop("src","<?php print_r(base_url()."assets/doc/pasien/");?>"+res_pemohon.val_response.foto);
                    $("#vert_nik").html(str_nik);
                    $("#vert_nama").html(res_pemohon.val_response.nama);
                    $("#vert_jk").html(str_jk);
                    $("#vert_no_hp").html(res_pemohon.val_response.no_hp);
                    $("#vert_ttl").html(res_pemohon.val_response.tmp_lhr+", "+res_pemohon.val_response.tgl_lhr);
                    $("#vert_alamat").html(res_pemohon.val_response.alamat+", "+res_pemohon.val_response.kelurahan+", "+res_pemohon.val_response.kecamatan+", "+res_pemohon.val_response.kab);
                    $("#vert_pekerjaan").html(res_pemohon.val_response.pekerjaan);

                    $("#vert_bertempat_dari").html(res_pemohon.val_response.bertempat_dari);
                    $("#vert_permintaan_dari").html(res_pemohon.val_response.permintaan_dari);
                    $("#vert_kesadaran").html(res_pemohon.val_response.kesadaran);
                    $("#vert_keumum").html(res_pemohon.val_response.keumum);
                    $("#vert_tekanandarah").html(res_pemohon.val_response.tekanandarah);
                    $("#vert_nadi").html(res_pemohon.val_response.nadi);
                    $("#vert_pernafasan").html(res_pemohon.val_response.pernafasan);
                    $("#vert_penggunaanobat").html(res_pemohon.val_response.penggunaanobat);
                    $("#vert_jenisobat").html(res_pemohon.val_response.jenisobat);
                    $("#vert_asalobat").html(res_pemohon.val_response.asalobat);
                    $("#vert_terakirminum").html(res_pemohon.val_response.terakirminum);
                    $("#vert_hasiltes").html(res_pemohon.val_response.hasiltes);
                    $("#vert_metode").html(res_pemohon.val_response.metode);
                    $("#vert_amphe").html(res_pemohon.val_response.amphe);
                    $("#vert_metha").html(res_pemohon.val_response.metha);
                    $("#vert_coco").html(res_pemohon.val_response.coco);
                    $("#vert_opioid").html(res_pemohon.val_response.opioid);
                    $("#vert_thc").html(res_pemohon.val_response.thc);
                    $("#vert_benzo").html(res_pemohon.val_response.benzo);
                    $("#vert_k2").html(res_pemohon.val_response.k2);
                    $("#vert_lain").html(res_pemohon.val_response.lain);
                    $("#vert_keperluan").html(res_pemohon.val_response.keperluan);
                    $("#vert_pemeriksa").html(res_pemohon.val_response.nama_pejabat);

                }else{
                    $("#vert_img_foto").prop("src","");
                    $("#vert_nik").html("");
                    $("#vert_nama").html("");
                    $("#vert_jk").html("");
                    $("#vert_no_hp").html("");
                    $("#vert_ttl").html("");
                    $("#vert_alamat").html("");
                    $("#vert_pekerjaan").html("");

                    $("#vert_bertempat_dari").html("");
                    $("#vert_permintaan_dari").html("");
                    $("#vert_kesadaran").html("");
                    $("#vert_keumum").html("");
                    $("#vert_tekanandarah").val("");
                    $("#vert_nadi").html("");
                    $("#vert_pernafasan").html("");
                    $("#vert_penggunaanobat").html("");
                    $("#vert_jenisobat").html("");
                    $("#vert_asalobat").html("");
                    $("#vert_terakirminum").html("");
                    $("#vert_hasiltes").html("");
                    $("#vert_metode").html("");
                    $("#vert_amphe").html("");
                    $("#vert_metha").html("");
                    $("#vert_coco").html("");
                    $("#vert_metodemetode").html("");
                    $("#vert_thc").html("");
                    $("#vert_benzo").html("");
                    $("#vert_k2").html("");
                    $("#vert_lain").html("");
                    $("#vert_keperluan").html("");
                    $("#vert_pemeriksa").html("");
                }
               
                id_vert_glob = id_admin;
            } else {
                clear_from_update();
            }
        }

        $("#btn_vert_data_data").click(function(){
            var data_main = new FormData();
            data_main.append('id', id_vert_glob);

            $.ajax({
                url: "<?php echo base_url()."pemeriksaan/get_vert/";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    // set_val_vert_data(res, id_admin);
                    // $("#vert_data_modal").modal('show');
                    response_vert_data(res);
                }
            });
        });

        function response_vert_data(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            swal({
                                title: "Proses Berhasil.!!",
                                text: "Verifikasi Data Pemeriksaan berhasil diubah ..!",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#28a745",
                                confirmButtonText: "Lanjutkan",
                                closeOnConfirm: false
                            }, function() {
                                window.location.href = "<?php echo base_url()."admin/data/pemeriksaan";?>";
                            });
                        },
                        //init
                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            } else {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            
                            swal("Proses Gagal.!!", "Verifikasi Data Pemeriksaan gagal diubah, coba periksa jaringan dan koneksi anda", "warning");
                        },

                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            }
        }

        // function vert_data(id_admin) {
            
        // }
    //=========================================================================//
    //-----------------------------------vert_data-----------------------------//
    //=========================================================================//


    $("#penggunaanobat").change(function(){
        penggunaanobat_ch();
    });

    function penggunaanobat_ch(){
        var val = $("#penggunaanobat").val();
        if(val == "tidak_ada"){
            $("#jenisobat").val("-");
            $("#jenisobat").prop("readonly", true);

            $("#asalobat").val("-");
            $("#asalobat").prop("readonly", true);

            $("#terakirminum").val("-");
            $("#terakirminum").prop("readonly", true);

        }else{
            // $("#penggunaanobat").val("resep dokter");

            $("#jenisobat").val("");
            $("#jenisobat").removeAttr("readonly", false);

            $("#asalobat").val("resep dokter");
            $("#asalobat").removeAttr("readonly", false);

            $("#terakirminum").val("");
            $("#terakirminum").removeAttr("readonly", false);
        }
    }

    $("#_penggunaanobat").change(function(){
        _penggunaanobat_ch();
    });

    function _penggunaanobat_ch(){
        var val = $("#_penggunaanobat").val();
        if(val == "tidak_ada"){
            $("#_jenisobat").val("-");
            $("#_jenisobat").prop("readonly", true);

            $("#_asalobat").val("-");
            $("#_asalobat").prop("readonly", true);

            $("#_terakirminum").val("-");
            $("#_terakirminum").prop("readonly", true);

        }else{
            // $("#penggunaanobat").val("resep dokter");

            $("#_jenisobat").val("");
            $("#_jenisobat").removeAttr("readonly", false);

            $("#_asalobat").val("resep dokter");
            $("#_asalobat").removeAttr("readonly", false);

            $("#_terakirminum").val("");
            $("#_terakirminum").removeAttr("readonly", false);
        }
    }

    function srt_data(id){
        window.location.href = "<?php echo base_url()."pemeriksaan/get_srt/";?>"+id;
    }
</script>