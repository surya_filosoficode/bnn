<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Data Kecamatan</h3>
    </div>
    
    
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Form Data Kecamatan</h4>

                </div>
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Nama Kecamatan :</label>
                                    <input type="text" class="form-control" id="kecamatan" name="kecamatan" required="">
                                    <p id="msg_kecamatan" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="col-md-2 text-right">
                                <div class="form-group">
                                    <label for="message-text" class="control-label"></label><br>
                                    <button type="submit" id="add_kecamatan" class="btn btn-success">Simpan</button>
                                    <p id="msg_add_kecamatan" style="color: red;"></p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->

    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Tabel Data Kecamatan</h4>

                </div>
                <div class="card-body">
                    <div class="card">
                        <div class="card-body">

                            <div class="table-responsive m-t-40">
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="10%">No. </th>
                                            <th width="70%">Nama Kecamatan</th>
                                            <th width="20%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            if(!empty($data_list)){
                                                foreach ($data_list as $key => $value) {
                                                    print_r("<tr>
                                                                <td>".($key+1)."</td>
                                                                <td>".$value->kecamatan."</td>
                                                                <td><button class=\"btn btn-info\" id=\"up_kec\" onclick=\"update_kec('".$value->id_kec."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;
                                                                    <button class=\"btn btn-danger\" id=\"del_kec\" onclick=\"delete_kec('".$value->id_kec."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                                </td>
                                                            </tr>");
                                                }
                                            }     
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="text-right">
                        <label class="form-label">Keterangan Tombol Aksi ==> </label>

                        <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                        <label class="form-label text-info">Update Data</label>,&nbsp;
                        <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                        <label class="form-label text-danger">Delete Data</label>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------update_admin------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="update_kec" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Ubah Data Kecamatan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <!-- <form action="<?= base_url()."admin_super/superadmin/update_admin";?>" method="post"> -->
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nama Kecamatan :</label>
                                <input type="text" class="form-control" id="_kecamatan" name="kecamatan" required="">
                                <p id="_msg_kecamatan" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="submit" id="btn_update_kecamatan" class="btn btn-success">Ubah Data</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------update_admin------------------------ -->
<!-- ============================================================== -->

<script type="text/javascript">
    var id_admin_glob = "";
    var key_ex, id_admin_op;

    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//
        $("#add_kecamatan").click(function() {
            var data_main = new FormData();
            data_main.append('kecamatan', $("#kecamatan").val());
           

            $.ajax({
                url: "<?php echo base_url()."master/insert_kec";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_insert(res);
                }
            });
        });

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            swal({
                                title: "Proses Berhasil.!!",
                                text: "Data Kecamatan berhasil disimpan ..!",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#28a745",
                                confirmButtonText: "Lanjutkan",
                                closeOnConfirm: false
                            }, function() {
                                window.location.href = "<?php echo base_url()."admin/data/kecamatan";?>";
                            });
                        },
                        //init
                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            } else {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            $("#msg_kecamatan").html(detail_msg.kecamatan);
                            

                            swal("Proses Gagal.!!", "Data Kecamatan gagal disimpan, coba periksa jaringan dan koneksi anda", "warning");
                        },

                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);

            }
        }
    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//
        function update_kec(id_admin) {
            var id_admin_chahce = "";
            clear_from_update();

            var data_main = new FormData();
            data_main.append('id_kec', id_admin);

            $.ajax({
                url: "<?php echo base_url()."master/get_kec_update";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    set_val_update(res, id_admin);
                    $("#update_kec").modal('show');
                }
            });
        }

        function set_val_update(res, id_admin) {
            var res_pemohon = JSON.parse(res);
            console.log(res_pemohon);
            if (res_pemohon.status == true) {
                id_admin_chahce = res_pemohon.val_response.id_admin;
                $("#_kecamatan").val(res_pemohon.val_response.kecamatan);
                
               
                id_admin_glob = id_admin;
            } else {
                clear_from_update();
            }
        }

        function clear_from_update() {
            $("#_kecamatan").val("");

            id_admin_glob = "";
            $("#update_admin").modal('hide');
        }
    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//
        $("#btn_update_kecamatan").click(function() {
            var data_main = new FormData();
            data_main.append('id_kec', id_admin_glob);
            data_main.append('kecamatan', $("#_kecamatan").val());
            

            $.ajax({
                url: "<?php echo base_url()."master/update_kec";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_update(res);
                }
            });
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            swal({
                                title: "Proses Berhasil.!!",
                                text: "Data Kecamatan berhasil diubah ..!",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#28a745",
                                confirmButtonText: "Lanjutkan",
                                closeOnConfirm: false
                            }, function() {
                                window.location.href = "<?php echo base_url()."admin/data/kecamatan";?>";
                            });
                        },
                        //init
                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            } else {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            $("#_msg_kecamatan").html(detail_msg.kecamatan);
                           
                            
                            swal("Proses Gagal.!!", "Data Kecamatan gagal diubah, coba periksa jaringan dan koneksi anda", "warning");
                        },

                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            }
        }
    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------admin_delete--------------------------//
    //=========================================================================//
        function delete_kec(id_admin) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({
                            title: "Pesan Konfirmasi",
                            text: "Silahkan Cermati data sebelem di hapus, jika anda sudah yakin maka data ini dan seluruh data yang berkaitan akan di hapus",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#ffb22b",
                            confirmButtonText: "Hapus",
                            closeOnConfirm: false
                        }, function() {
                            var data_main = new FormData();
                            data_main.append('id_kec', id_admin);

                            $.ajax({
                                url: "<?php echo base_url()."master/delete_kec/";?>",
                                dataType: 'html', // what to expect back from the PHP script, if anything
                                cache: false,
                                contentType: false,
                                processData: false,
                                data: data_main,
                                type: 'post',
                                success: function(res) {
                                    // console.log(id_admin);
                                    response_delete(res);
                                }
                            });
                        });
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }

        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");

                swal({
                    title: "Proses Berhasil.!!",
                    text: "Data Kecamatan berhasil dihapus ..!",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#28a745",
                    confirmButtonText: "Lanjutkan",
                    closeOnConfirm: false
                }, function() {
                    window.location.href = "<?php echo base_url()."admin/data/kecamatan";?>";
                });

            } else {
                // console.log("false");
                swal("Proses Gagal.!!", "Data Kecamatan gagal dihapus, coba periksa jaringan dan koneksi anda", "warning");
            }
        }
    //=========================================================================//
    //-----------------------------------admin_update--------------------------//
    //=========================================================================//

</script>