
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?=base_url()?>assets/img/logo_bnn.png">
    <title>admin_login</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?=base_url()?>template_master/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?=base_url()?>template_master/main/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?=base_url()?>template_master/main/css/colors/purple.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<?php
    $response_login = "";
    if(isset($_SESSION["response_login"])){
        $response_login = $this->session->flashdata("response_login")["msg_main"]["msg"];
        print_r($response_login);
    }
    
?>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper" class="login-register" style="background-image:url(<?=base_url()?>assets/img/background1.png);">
        <div class="col-md-2">
            <img style="width: 150px; height: 150px" src="<?=base_url()?>assets/img/logo_bnn.png" alt="Home"/>
        </div>
        <div class="login-box card">
            <form method="post" action="<?php print_r(base_url()."login/auth");?>">
                <div class="card-body">
                    <div class="form-horizontal form-material" id="loginform" action="index.html">
                        <!-- <a href="javascript:void(0)" class="text-center db"> -->
                            <!-- <img style="width: 150px; height: 150px" src="<?=base_url()?>assets/img/logo_bnn.png" alt="Home"/><br/> -->
                            <!-- <img src="<?=base_url()?>template_master/assets/images/logo-text.png" alt="Home" /></a> -->
                            <h3 class="box-title m-b-20">Login Admin</h3>
                        <div class="form-group m-t-40">
                            <div class="col-xs-12">
                                <input class="form-control" type="text" name="email" id="email" required="" placeholder="Username Or Email">
                                <!-- <p id="email_msg" style="color: red;">aaa</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" type="password" name="password" id="password" required="" placeholder="Password">
                                <!-- <p id="password_msg" style="color: red;">aaa</p> -->
                            </div>
                        </div>
                        <br>

                        <p id="password_msg" style="color: red;"><?=$response_login;?></p>
                        <hr>
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
                            </div>
                        </div>
                        
                        <!-- <div class="form-group m-b-0">
                            <div class="col-sm-12 text-center">
                                <p>lupa password anda ..? <a href="pages-register2.html" class="text-primary m-l-5"><b>Dapatkan Password</b></a></p>
                            </div>
                        </div> -->
                    </div>
                </div>
            </form>
        </div>
    </section>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?=base_url()?>template_master/assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?=base_url()?>template_master/assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="<?=base_url()?>template_master/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?=base_url()?>template_master/main/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?=base_url()?>template_master/main/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?=base_url()?>template_master/main/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?=base_url()?>template_master/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="<?=base_url()?>template_master/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?=base_url()?>template_master/main/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?=base_url()?>template_master/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>