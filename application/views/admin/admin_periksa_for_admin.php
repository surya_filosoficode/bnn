<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Data Pemohon</h3>
    </div>
    
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    

    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Tabel Data Pemohon</h4>

                </div>
                <div class="card-body">
                    <div class="card">
                        <div class="card-body">

                            <div class="table-responsive m-t-40">
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="5%">No. </th>
                                            <th width="10%">Tgl. Pendaftaran</th>
                                            <th width="20%">Nama Pemohon</th>
                                            <th width="15%">Identitas</th>
                                            <th width="25%">Alamat</th>
                                            <th width="15%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            if(!empty($data_list)){
                                                // print_r($data_list);
                                                foreach ($data_list as $key => $value) {
                                                    // print_r("<pre>");
                                                    // print_r($value);

                                                    $str_identitas = "";

                                                    $str_j_iden = "KTP";
                                                    if($value->jenis_identitas == "1"){
                                                        $str_j_iden = "SIM";
                                                    }elseif ($value->jenis_identitas == "2") {
                                                        $str_j_iden = "KTM";
                                                    }

                                                    $str_identitas = "(".$str_j_iden.") ".$value->no_identitas;


                                                    $str_alamat = $value->alamat.", ".$value->kelurahan.", ".$value->kecamatan.", ".$value->kab;
                                                    print_r("<tr>
                                                                <td>".($key+1)."</td>
                                                                <td>".$value->tgl_daftar."</td>
                                                                <td>".$value->nama."</td>
                                                                <td>".$str_identitas."</td>
                                                                <td>".$str_alamat."</td>
                                                                <td><button class=\"btn btn-info\" id=\"up_data\" onclick=\"update_data('".$value->id_data."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;
                                                                    <button class=\"btn btn-danger\" id=\"del_data\" onclick=\"delete_data('".$value->id_data."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>&nbsp;&nbsp;
                                                                    <button class=\"btn btn-success\" id=\"nil_data\" onclick=\"nilai_data('".$value->id_data."')\" style=\"width: 40px;\"><i class=\"fa fa-list-alt\"></i></button>
                                                                </td>
                                                            </tr>");
                                                }
                                            }     
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="text-right">
                        <label class="form-label">Keterangan Tombol Aksi ==> </label>

                        <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                        <label class="form-label text-info">Update Data</label>,&nbsp;
                        <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                        <label class="form-label text-danger">Delete Data</label>
                        <a class="btn btn-success" style="width: 40px;"><i class="fa fa-list-alt" style="color: white;"></i></a>
                        <label class="form-label text-success">Periksa Pasien</label>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------update_admin------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="update_data" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Ubah Data Pasien</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <!-- <form action="<?= base_url()."admin_super/superadmin/update_admin";?>" method="post"> -->
            <div class="modal-body">
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Foto</label>
                                    <input type="file" class="form-control" id="_foto" name="foto" required="">
                                    <p id="_msg_foto" style="color: red;"></p><br>
                                    <center>
                                        <img src="" id="_img_foto" style="width:162px ;height:237px ;">
                                    </center>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">Jenis Identitas</label>

                                            <select class="form-control" id="_jenis_identitas" name="jenis_identitas">
                                                <option value="0">KTP</option>
                                                <option value="1">SIM</option>
                                                <option value="2">Kartu Tanda Mahasiswa</option>
                                            </select>
                                            <p id="_msg_jenis_identitas" style="color: red;"></p>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">No. Identitas</label>
                                            <input type="number" class="form-control" id="_no_identitas" name="no_identitas" required="">
                                            <p id="_msg_no_identitas" style="color: red;"></p>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">Nama Pemohon</label>
                                            <input type="text" class="form-control" id="_nama" name="nama" required="">
                                            <p id="_msg_nama" style="color: red;"></p>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">Jenis Kelamin</label>
                                            <!-- <input type="text" class="form-control" id="jk" name="jk" required=""> -->
                                            <select class="form-control" id="_jk" name="jk" >
                                                <option value="0">Laki-Laki</option>
                                                <option value="1">Perempuan</option>
                                            </select>
                                            <p id="_msg_jk" style="color: red;"></p>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">No. Hp Pemohon</label>
                                            <input type="number" class="form-control" id="_no_hp" name="no_hp" required="">
                                            <p id="_msg_no_hp" style="color: red;"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                            
                            
                            
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Tempat Lahir</label>
                                    <input type="text" class="form-control" id="_tmp_lhr" name="tmp_lhr" required="">
                                    <p id="_msg_tmp_lhr" style="color: red;"></p>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Tgl. Lahir</label>
                                    <input type="date" class="form-control" id="_tgl_lhr" name="tgl_lhr" required="">
                                    <p id="_msg_tgl_lhr" style="color: red;"></p>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Alamat</label>
                                    <input type="text" class="form-control" id="_alamat" name="alamat" required="">
                                    <p id="_msg_alamat" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Pekerjaan</label>
                                    <input type="text" class="form-control" id="_pekerjaan" name="pekerjaan" required="">
                                    <p id="_msg_pekerjaan" style="color: red;"></p>
                                </div>
                            </div>


                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Kecamatan</label>
                                    <!-- <input type="text" class="form-control" id="kec" name="kec" required=""> -->

                                    <select class="form-control" id="_kec" name="kec">
                                        
                                        <?php 
                                        if($data_kec){
                                            foreach ($data_kec as $key => $value) {
                                                print_r("<option value=\"".$value->id_kec."\">".$value->kecamatan."</option>");
                                            }
                                        }
                                            
                                        ?>
                                    </select>
                                    <p id="_msg_kec" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Kelurahan</label>
                                    <!-- <input type="text" class="form-control" id="kel" name="kel" required=""> -->
                                    <select class="form-control"  id="_kel" name="kel" required="">
                                        <?php 
                                        // if($data_kel){
                                        //     foreach ($data_kel as $key => $value) {
                                        //         print_r("<option value=\"".$value->id_kel."\">".$value->kelurahan."</option>");
                                        //     }
                                        // }
                                            
                                        ?>
                                    </select>
                                    <p id="_msg_kel" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Kabupaten</label>
                                    <!-- <input type="text" class="form-control" id="kab" name="kab" required=""> -->
                                    <select class="form-control"  id="_kab" name="kab" required="">
                                        <?php 
                                        if($data_kab){
                                            foreach ($data_kab as $key => $value) {
                                                print_r("<option value=\"".$value->id_kab."\">".$value->kab."</option>");
                                            }
                                        }
                                            
                                        ?>
                                    </select>
                                    <p id="_msg_kab" style="color: red;"></p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="submit" id="btn_update_data" class="btn btn-success">Ubah Data</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------update_admin------------------------ -->
<!-- ============================================================== -->

<script type="text/javascript">
    var id_admin_glob = "";
    var key_ex, id_admin_op;

    var data_kel = JSON.parse('<?php print_r($data_kel);?>');

    $(document).ready(function(){
        var val_kec = $("#kec").val();
        var str_data_kel = get_kel(val_kec);

        $("#kel").html(str_data_kel);


        var _val_kec = $("#_kec").val();
        var _str_data_kel = get_kel(val_kec);

        $("#_kel").html(_str_data_kel);
    });


    $("#kec").change(function(){
        var val_kec = $("#kec").val();
        var str_data_kel = get_kel(val_kec);

        $("#kel").html(str_data_kel);
    });

    $("#_kec").change(function(){
        var val_kec = $("#_kec").val();
        var str_data_kel = get_kel(val_kec);

        $("#_kel").html(str_data_kel);
    });

    function get_kel(val_kec){
        var data_main = data_kel[val_kec];

        // console.log(data_main);
        var str_data_kel = "";
        for (let i in data_main) {
            str_data_kel += "<option value=\""+data_main[i].id_kel+"\">"+data_main[i].kelurahan+"</option>";
        }

        return str_data_kel;
    }


    //=========================================================================//
    //-----------------------------------nilai_data----------------------------//
    //=========================================================================//
        function nilai_data(id_data){
            window.location.href = "<?php print_r(base_url()); ?>admin/data/pemeriksaan_all/admin/"+id_data;
        }
    //=========================================================================//
    //-----------------------------------nilai_data----------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//

        var file = [];
        $("#foto").change(function(e){
            file = e.target.files[0];

            $("#img_foto").attr("src",URL.createObjectURL(file));
            console.log(file);
        });

        $("#add_pasien").click(function() {
            var data_main = new FormData();
            data_main.append('jenis_identitas', $("#jenis_identitas").val());
            data_main.append('no_identitas', $("#no_identitas").val());
            data_main.append('no_hp', $("#no_hp").val());
            data_main.append('nama', $("#nama").val());
            data_main.append('alamat', $("#alamat").val());
            data_main.append('jk', $("#jk").val());
            data_main.append('tmp_lhr', $("#tmp_lhr").val());
            data_main.append('tgl_lhr', $("#tgl_lhr").val());
            data_main.append('kel', $("#kel").val());
            data_main.append('kec', $("#kec").val());
            data_main.append('kab', $("#kab").val());
            data_main.append('pekerjaan', $("#pekerjaan").val());
            data_main.append('foto', file);
           

            $.ajax({
                url: "<?php echo base_url()."periksa/insert_periksa";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_insert(res);
                }
            });
        });

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            swal({
                                title: "Proses Berhasil.!!",
                                text: "Data Pemohon berhasil disimpan ..!",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#28a745",
                                confirmButtonText: "Lanjutkan",
                                closeOnConfirm: false
                            }, function() {
                                window.location.href = "<?php echo base_url()."admin/data/periksa";?>";
                            });
                        },
                        //init
                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            } else {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            $("#msg_jenis_identitas").html(detail_msg.jenis_identitas);
                            $("#msg_no_identitas").html(detail_msg.no_identitas);
                            $("#msg_foto").html(detail_msg.foto);
                            $("#msg_no_hp").html(detail_msg.no_hp);
                            $("#msg_nama").html(detail_msg.nama);
                            $("#msg_alamat").html(detail_msg.alamat);
                            $("#msg_jk").html(detail_msg.jk);
                            $("#msg_tmp_lhr").html(detail_msg.tmp_lhr);
                            $("#msg_tgl_lhr").html(detail_msg.tgl_lhr);
                            $("#msg_kel").html(detail_msg.kel);
                            $("#msg_kec").html(detail_msg.kec);
                            $("#msg_kab").html(detail_msg.kab);
                            $("#msg_pekerjaan").html(detail_msg.pekerjaan);


                            

                            swal("Proses Gagal.!!", "Data Pemohon gagal disimpan, coba periksa jaringan dan koneksi anda", "warning");
                        },

                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);

            }
        }
    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//
        function update_data(id_admin) {
            var id_admin_chahce = "";
            clear_from_update();

            var data_main = new FormData();
            data_main.append('id_data', id_admin);

            $.ajax({
                url: "<?php echo base_url()."periksa/get_periksa_update";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    set_val_update(res, id_admin);
                    $("#update_data").modal('show');
                }
            });
        }

        function set_val_update(res, id_admin) {
            var res_pemohon = JSON.parse(res);
            console.log(res_pemohon);
            if (res_pemohon.status == true) {
                id_admin_chahce = res_pemohon.val_response.id_data;
                $("#_jenis_identitas").val(res_pemohon.val_response.jenis_identitas);
                $("#_no_identitas").val(res_pemohon.val_response.no_identitas);
                $("#_no_hp").val(res_pemohon.val_response.no_hp);
                $("#_nama").val(res_pemohon.val_response.nama);
                $("#_alamat").val(res_pemohon.val_response.alamat);
                $("#_jk").val(res_pemohon.val_response.jk);
                $("#_tmp_lhr").val(res_pemohon.val_response.tmp_lhr);
                $("#_tgl_lhr").val(res_pemohon.val_response.tgl_lhr);
                $("#_kel").val(res_pemohon.val_response.kel);
                $("#_kec").val(res_pemohon.val_response.kec);
                $("#_kab").val(res_pemohon.val_response.kab);
                $("#_pekerjaan").val(res_pemohon.val_response.pekerjaan);


                $("#_img_foto").attr('src', res_pemohon.url_base+res_pemohon.val_response.foto)
                
               
                id_admin_glob = id_admin;
            } else {
                clear_from_update();
            }
        }

        function clear_from_update() {
            $("#_jenis_identitas").val("");
            $("#_no_identitas").val("");
            $("#_img_foto").val("");
            $("#_no_hp").val("");
            $("#_nama").val("");
            $("#_alamat").val("");
            $("#_jk").val("");
            $("#_tmp_lhr").val("");
            $("#_tgl_lhr").html("");
            $("#_kel").val("");
            $("#_kec").val("");
            $("#_kab").val("");
            $("#_pekerjaan").val("");

            id_admin_glob = "";
            $("#update_data").modal('hide');
        }
    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//

        var file = [];
        $("#_foto").change(function(e){
            file = e.target.files[0];

            $("#_img_foto").attr("src",URL.createObjectURL(file));
            console.log(file);
        });

        $("#btn_update_data").click(function() {
            var data_main = new FormData();
            data_main.append('id_data', id_admin_glob);
            data_main.append('jenis_identitas', $("#_jenis_identitas").val());
            data_main.append('no_identitas', $("#_no_identitas").val());
            data_main.append('no_hp', $("#_no_hp").val());
            data_main.append('nama', $("#_nama").val());
            data_main.append('alamat', $("#_alamat").val());
            data_main.append('jk', $("#_jk").val());
            data_main.append('tmp_lhr', $("#_tmp_lhr").val());
            data_main.append('tgl_lhr', $("#_tgl_lhr").val());
            data_main.append('kel', $("#_kel").val());
            data_main.append('kec', $("#_kec").val());
            data_main.append('kab', $("#_kab").val());
            data_main.append('pekerjaan', $("#_pekerjaan").val());
            data_main.append('foto', file);
            

            $.ajax({
                url: "<?php echo base_url()."periksa/update_periksa";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_update(res);
                }
            });
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            swal({
                                title: "Proses Berhasil.!!",
                                text: "Data Pemohon berhasil diubah ..!",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#28a745",
                                confirmButtonText: "Lanjutkan",
                                closeOnConfirm: false
                            }, function() {
                                window.location.href = "<?php echo base_url()."admin/data/periksa";?>";
                            });
                        },
                        //init
                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            } else {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            
                        $("#_msg_jenis_identitas").val("");
                        $("#_msg_no_identitas").val("");
                        $("#_msg_foto").val("");
                        $("#_msg_no_hp").val("");
                        $("#_msg_nama").val("");
                        $("#_msg_alamat").val("");
                        $("#_msg_jk").val("");
                        $("#_msg_tmp_lhr").val("");
                        $("#_msg_tgl_lhr").html("");
                        $("#_msg_kel").val("");
                        $("#_msg_kec").val("");
                        $("#_msg_kab").val("");
                        $("#_msg_pekerjaan").val("");
                           
                            
                            swal("Proses Gagal.!!", "Data Pemohon gagal diubah, coba periksa jaringan dan koneksi anda", "warning");
                        },

                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            }
        }
    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------admin_delete--------------------------//
    //=========================================================================//
        function delete_data(id_admin) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({
                            title: "Pesan Konfirmasi",
                            text: "Silahkan Cermati data sebelem di hapus, jika anda sudah yakin maka data ini dan seluruh data yang berkaitan akan di hapus",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#ffb22b",
                            confirmButtonText: "Hapus",
                            closeOnConfirm: false
                        }, function() {
                            var data_main = new FormData();
                            data_main.append('id_data', id_admin);

                            $.ajax({
                                url: "<?php echo base_url()."periksa/delete_periksa/";?>",
                                dataType: 'html', // what to expect back from the PHP script, if anything
                                cache: false,
                                contentType: false,
                                processData: false,
                                data: data_main,
                                type: 'post',
                                success: function(res) {
                                    // console.log(id_admin);
                                    response_delete(res);
                                }
                            });
                        });
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }

        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");

                swal({
                    title: "Proses Berhasil.!!",
                    text: "Data Pemohon berhasil dihapus ..!",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#28a745",
                    confirmButtonText: "Lanjutkan",
                    closeOnConfirm: false
                }, function() {
                    window.location.href = "<?php echo base_url()."admin/data/periksa";?>";
                });

            } else {
                // console.log("false");
                swal("Proses Gagal.!!", "Data Pemohon gagal dihapus, coba periksa jaringan dan koneksi anda", "warning");
            }
        }
    //=========================================================================//
    //-----------------------------------admin_update--------------------------//
    //=========================================================================//

</script>