<?php
	date_default_timezone_set("Asia/Bangkok");
	$month = array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
	$day = array("Sunday"=>"Minggu",
					"Monday"=>"Senin",
					"Tuesday"=>"Selasa",
					"Wednesday"=>"Rabu",
					"Thursday"=>"Kamis",
					"Friday"=>"Jumat",
					"Saturday"=>"Sabtu");
	$id = "";
	$waktu = "";
	$bertempat_dari = "";
	$srt_pengantar = "";
	$permintaan_dari = "";
	$nomer_surat = "";
	$status = "";
	$id_pasien = "";
	$kesadaran = "";
	$keumum = "";
	$tekanandarah = "";
	$nadi = "";
	$pernafasan = "";
	$penggunaanobat = "";
	$jenisobat = "";
	$asalobat = "";
	$terakirminum = "";
	$hasiltes = "";
	$metode = "";
	$amphe = "";
	$metha = "";
	$coco = "";
	$opioid = "";
	$thc = "";
	$benzo = "";
	$k2 = "";
	$lain = "";
	$keperluan = "";
	$id_data = "";
	$tgl_daftar = "";
	$jenis_identitas = "";
	$no_identitas = "";
	$foto = "";
	$no_hp = "";
	$nama = "";
	$alamat = "";
	$jk = "";
	$tmp_lhr = "";
	
	$tgl_lhr = "";
	$kel = "";
	$kec = "";
	$kab = "";
	$pekerjaan = "";
	$id_kec = "";
	$kecamatan = "";
	$id_kel = "";
	$kelurahan = "";
	$id_kab = "";
	$waktu_srt = "";
	$waktu_srt_fix = "";

	$nama_pejabat = "";
	$nip_pejabat = "";
	$mengetahui = "";

	$array_mengetahui = array();

	if(isset($detail)){
		// print_r($detail);
		$id = $detail["id"];
		$waktu = $detail["waktu"];
		$bertempat_dari = $detail["bertempat_dari"];
		$permintaan_dari = $detail["permintaan_dari"];
		$srt_pengantar = $detail["srt_pengantar"];
		$nomer_surat = $detail["nomer_surat"];
		$status = $detail["status"];
		$id_pasien = $detail["id_pasien"];
		$kesadaran = $detail["kesadaran"];
		$keumum = $detail["keumum"];
		$tekanandarah = $detail["tekanandarah"];
		$nadi = $detail["nadi"];

		$pernafasan = $detail["pernafasan"];
		$penggunaanobat = $detail["penggunaanobat"];
		$jenisobat = $detail["jenisobat"];
		$asalobat = $detail["asalobat"];
		$terakirminum = $detail["terakirminum"];
		$hasiltes = $detail["hasiltes"];
		$metode = $detail["metode"];
		$amphe = $detail["amphe"];
		$metha = $detail["metha"];
		$coco = $detail["coco"];
		$opioid =$detail["opioid"];
		$thc = $detail["thc"];
		$benzo = $detail["benzo"];
		$k2 = $detail["k2"];
		$lain = $detail["lain"];
		$keperluan = $detail["keperluan"];
		$id_data = $detail["id_data"];
		$tgl_daftar = $detail["tgl_daftar"];
		$jenis_identitas = $detail["jenis_identitas"];
		$no_identitas = $detail["no_identitas"];
		$foto = $detail["foto"];
		$no_hp = $detail["no_hp"];
		$nama = $detail["nama"];
		$alamat = $detail["alamat"];
		$jk = $detail["jk"];
		$tmp_lhr = $detail["tmp_lhr"];
		
		$tgl_lhr = $detail["tgl_lhr"];
		$kel = $detail["kel"];
		$kec = $detail["kec"];
		$kab = $detail["kab"];
		$pekerjaan = $detail["pekerjaan"];
		$id_kec = $detail["id_kec"];
		$kecamatan = $detail["kecamatan"];
		$id_kel = $detail["id_kel"];
		$kelurahan = $detail["kelurahan"];
		$id_kab = $detail["id_kab"];

		$nama_pejabat = $detail["nama_pejabat"];
		$nip_pejabat = $detail["nip_pejabat"];
		$mengetahui = $detail["mengetahui"];

		$array_mengetahui = $mengetahui_data[(int)$mengetahui];
		// if($mengetahui == "1"){

		// }

		$waktu_srt = explode(" ", $waktu)[1];
		$waktu_srt_fix = explode(":", $waktu_srt)[0].":".explode(":", $waktu_srt)[1];


		$str_no_srt = "SKET/".((int)substr($nomer_surat, 6, 6)+0)."/".$data_month_rom[(int)substr($nomer_surat, 12, 2)+0].(substr($nomer_surat, 14, 15));

		// print_r($str_no_srt);
		// print_r(substr($nomer_surat, 12, 2));

		$explode_tgl_daftar = explode("-", explode(" ", $waktu)[0]);
		$str_tgl_daftar = (int) $explode_tgl_daftar[2]. " " . $month[(int) $explode_tgl_daftar[1]]. " " .(int) $explode_tgl_daftar[0];

		$str_day = $day[date("l", strtotime($tgl_daftar))];

		$str_jk = ($jk == "0") ? "Laki-Laki" : "Perampuan";

		$date_now = new DateTime();
		$str_umur = $date_now->diff(new DateTime($tgl_lhr));

		$explode_date = explode("-", $tgl_lhr);
		$str_ttl = $tmp_lhr." / ". (int) $explode_date[2]. " " . $month[(int) $explode_date[1]]. " " .(int) $explode_date[0];


		$str_kesadaran = "Baik/<strike>Terganggu</strike>";
		if($kesadaran == "terganggu"){
			$str_kesadaran = "<strike>Baik</strike>/Terganggu";
		}


		$str_keadaan_umum = "Baik/<strike>Cukup/Kurang</strike>";
		if($keumum == "cukup"){
			$str_keadaan_umum = "<strike>Baik</strike>/Cukup/<strike>Kurang</strike>";
		}elseif ($keumum == "kurang"){
			$str_keadaan_umum = "<strike>Baik/Cukup</strike>/Kurang";
		}

		$str_penggunaanobat = "ADA/<strike>TIDAK ADA</strike>";
		if($penggunaanobat == "tidak_ada"){
			$str_penggunaanobat = "<strike>ADA</strike>/TIDAK ADA";
		}



		$array_kesimpulan = array();

		$str_amphe = "POSITIF/<strike>NEGATIF</strike>";
		if($amphe == "negatif"){
			$str_amphe = "<strike>POSITIF</strike>/NEGATIF";
		}else{
			array_push($array_kesimpulan, "Amphetamine");
		}

		$str_metha = "POSITIF/<strike>NEGATIF</strike>";
		if($metha == "negatif"){
			$str_metha = "<strike>POSITIF</strike>/NEGATIF";
		}else{
			array_push($array_kesimpulan, "Methahetamine");
		}

		$str_coco = "POSITIF/<strike>NEGATIF</strike>";
		if($coco == "negatif"){
			$str_coco = "<strike>POSITIF</strike>/NEGATIF";
		}else{
			array_push($array_kesimpulan, "Cocaine");
		}

		$str_opioid = "POSITIF/<strike>NEGATIF</strike>";
		if($opioid == "negatif"){
			$str_opioid = "<strike>POSITIF</strike>/NEGATIF";
		}else{
			array_push($array_kesimpulan, "Opioid");
		}

		$str_thc = "POSITIF/<strike>NEGATIF</strike>";
		if($thc == "negatif"){
			$str_thc = "<strike>POSITIF</strike>/NEGATIF";
		}else{
			array_push($array_kesimpulan, "THC");
		}

		$str_benzo = "POSITIF/<strike>NEGATIF</strike>";
		if($benzo == "negatif"){
			$str_benzo = "<strike>POSITIF</strike>/NEGATIF";
		}else{
			array_push($array_kesimpulan, "Benzodiazepine");
		}

		$str_k2 = "POSITIF/<strike>NEGATIF</strike>";
		if($k2 == "negatif"){
			$str_k2 = "<strike>POSITIF</strike>/NEGATIF";
		}else{
			array_push($array_kesimpulan, "K2");
		}

		if($lain != "" and $lain != "-"){
			array_push($array_kesimpulan, ucwords($lain));
		}

		$str_kesimpulan_main = "<strike>Terindikasi</strike>/Tidak Terindikasi";
		if($array_kesimpulan){
			$str_kesimpulan_main = "Terindikasi/<strike>Tidak Terindikasi</strike>";
		}

		$str_kesimpulan_obat = implode(", ", $array_kesimpulan);
	}



	


?>



<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		.font_arial{
			font-family:Arial;
		}
	</style>
</head>
<body>
	<table border="0" width="100%" class="font_arial">
		<tr>
			<td align="center" rowspan="3"><img src="<?=base_url()?>assets/img/logo1.jpg" style="width:100px ;height:100px ;"></td>
			<td align="center">
				<b>BADAN NARKOTIKA NASIONAL REPUBLIK INDONSIA</b>
			</td>
		</tr>
		<tr>
			<td align="center">
				<b>(NASTIONAL NARCOTICS BOARD REPUBLIC OF INDONESIA)</b>
			</td>
		</tr>
		<tr>
			<td align="center">
				JL MT Haryono. Np. 11, Cawang, Jakarta Timur<br>
				Telephon : (62-21) 80871566, 80871567<br>
			</td>
		</tr>

		<tr>
			<td align="center" colspan="2">
				<hr>
			</td>
		</tr>
		<tr>
			<td align="center" colspan="2">
				&nbsp;
			</td>
		</tr>

		<tr>
			<td align="center" colspan="2">
				<b><u>SURAT KETERANGAN PEMERIKSAAN NARKOTIKA</u></b>
			</td>
		</tr>
		<tr>
			<td align="center" colspan="2">
				<h4>No: <?=$str_no_srt;?></h4>
			</td>
		</tr>

		<tr>
			<td align="left" colspan="2" style="text-align: justify;">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				Yang bertandatangan di bawah ini menerangkan bahwa, pada Hari <?php print_r($str_day);?>, Tanggal <?php print_r($str_tgl_daftar);?> Pukul <?php print_r($waktu_srt_fix);?> bertempat di klinik Pratama <?php print_r($bertempat_dari);?>, atas dasar permintaan dari <?php print_r($permintaan_dari);?> dengan No Surat <?php print_r($srt_pengantar);?> telah di lakukan pemeriksaan terhadap:
			</td>
		</tr>
	</table>

	<br>
	<table border="0" width="100%" class="font_arial">
		<tr>
			<td width="25%" align="left">Nama</td>
			<td width="2%" align="center">:</td>
			<td width="*" align="left"><?php print_r($nama);?></td>
		</tr>
		<tr>
			<td width="25%" align="left">Jenis Kelamin</td>
			<td width="2%" align="center">:</td>
			<td width="*" align="left"><?php print_r($str_jk);?></td>
		</tr>
		<tr>
			<td width="25%" align="left">Umur</td>
			<td width="2%" align="center">:</td>
			<td width="*" align="left"><?php print_r($str_umur->y);?> Tahun</td>
		</tr>
		<tr>
			<td width="25%" align="left">Tempat / Tanggal Lahir</td>
			<td width="2%" align="center">:</td>
			<td width="*" align="left"><?php print_r($str_ttl);?></td>
		</tr>
		<tr>
			<td width="25%" align="left">Alamat</td>
			<td width="2%" align="center">:</td>
			<td width="*" align="left"><?php print_r($alamat.", ".$kelurahan.", ".$kecamatan.", ".$kab);?></td>
		</tr>
		<tr>
			<td width="25%" align="left">Pekerjaan</td>
			<td width="2%" align="center">:</td>
			<td width="*" align="left"><?php print_r($pekerjaan);?></td>
		</tr>

		
	</table>

	<br><br>
	<table border="0" width="100%" class="font_arial">
		<tr>
			<td align="left"><b><u>Hasil Wawancara dan Pemeriksaan Fisik</u></b></td>
		</tr>
	</table>

	<br>
	<table border="0" width="100%" class="font_arial">
		<tr>
			<td width="3%" align="center">1.</td>
			<td width="25%" align="left">Kesadaran</td>
			<td width="2%" align="center">:</td>
			<td width="*" align="left"><?php print_r($str_kesadaran);?></td>
		</tr>
		<tr>
			<td width="3%" align="center">2.</td>
			<td width="25%" align="left">Keadaan Umum</td>
			<td width="2%" align="center">:</td>
			<td width="*" align="left"><?php print_r($str_keadaan_umum);?></td>
		</tr>
		<tr>
			<td width="3%" align="center">3.</td>
			<td width="25%" align="left">Tekanan Darah</td>
			<td width="2%" align="center">:</td>
			<td width="*" align="left"><?php print_r($tekanandarah);?>, mmHg</td>
		</tr>
		<tr>
			<td width="3%" align="center">4.</td>
			<td width="25%" align="left">Nadi</td>
			<td width="2%" align="center">:</td>
			<td width="*" align="left"><?php print_r($nadi);?>, x/Menit</td>
		</tr>
		<tr>
			<td width="3%" align="center">5.</td>
			<td width="25%" align="left">Pernafasaan</td>
			<td width="2%" align="center">:</td>
			<td width="*" align="left"><?php print_r($pernafasan);?>, x/Menit</td>
		</tr>
	</table>

	<br><br>
	<table border="0" width="100%" class="font_arial">
		<tr>
			<td align="left"><b><u>Riwayat Penggunaan Obat-obatan Dalam Seminggu Terakhir</u></b></td>
		</tr>
	</table>

	<br>
	<table border="0" width="100%" class="font_arial">
		<tr>
			<td width="3%" align="center">1.</td>
			<td width="*" align="left">Pengunaan Obat dalam seminggu ini: <?php print_r($str_penggunaanobat);?> (Bila ada, lanjut ke pertanyaan berikutnya.)</td>

		</tr>
		<tr>
			<td width="3%" align="center">2.</td>
			<td width="*" align="left">Jenis Obat yang digunakan: <?php print_r($jenisobat);?></td>
		</tr>
		<tr>
			<td width="3%" align="center">3.</td>
			<td width="*" align="left">Asal Obat: <?php print_r($asalobat);?></td>
		</tr>
		<tr>
			<td width="3%" align="center">4.</td>
			<td width="*" align="left">Terakhir minum : <?php print_r($terakirminum);?> hari yang lalu</td>
		</tr>
	</table>


	<br><br><br><br><br><br><br><br>
	<table border="0" width="100%" class="font_arial">
		<tr>
			<td align="left"><b><u>Hasil Tes Urine Oleh:</u></b></td>
		</tr>
		<tr>
			<td align="left">Pemeriksaan umum dengan methode : <?php print_r(ucwords($metode));?></td>
		</tr>
	</table>

	<br>
	<table border="0" width="100%" class="font_arial">
		<tr>
			<td width="3%" align="center">a.</td>
			<td width="25%" align="left">Amphetamine</td>
			<td width="2%" align="center">:</td>
			<td width="*" align="left"><?php print_r($str_amphe);?></td>
		</tr>
		<tr>
			<td width="3%" align="center">b.</td>
			<td width="25%" align="left">Methahetamine</td>
			<td width="2%" align="center">:</td>
			<td width="*" align="left"><?php print_r($str_metha);?></td>
		</tr>
		<tr>
			<td width="3%" align="center">c.</td>
			<td width="25%" align="left">Cocaine</td>
			<td width="2%" align="center">:</td>
			<td width="*" align="left"><?php print_r($str_coco);?></td>
		</tr>
		<tr>
			<td width="3%" align="center">d.</td>
			<td width="25%" align="left">Opioid</td>
			<td width="2%" align="center">:</td>
			<td width="*" align="left"><?php print_r($str_opioid);?></td>
		</tr>
		<tr>
			<td width="3%" align="center">e.</td>
			<td width="25%" align="left">THC</td>
			<td width="2%" align="center">:</td>
			<td width="*" align="left"><?php print_r($str_thc);?></td>
		</tr>
		<tr>
			<td width="3%" align="center">e.</td>
			<td width="25%" align="left">Benzodiazepine</td>
			<td width="2%" align="center">:</td>
			<td width="*" align="left"><?php print_r($str_benzo);?></td>
		</tr>
		<tr>
			<td width="3%" align="center">e.</td>
			<td width="25%" align="left">K2</td>
			<td width="2%" align="center">:</td>
			<td width="*" align="left"><?php print_r($str_k2);?></td>
		</tr>
		<tr>
			<td width="3%" align="center">e.</td>
			<td width="25%" align="left">Lain-Lain: </td>
			<td width="2%" align="center">:</td>
			<td width="*" align="left"><?php print_r($lain);?></td>
		</tr>
	</table>


	<br><br>
	<table border="0" width="100%" class="font_arial">
		
		<tr>
			<td align="left" colspan="2" style="text-align: justify;">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				Dapat disimpulkan bahwa yang terperiksa tersebut diatas "<?php print_r($str_kesimpulan_main);?>" mengkonsumsi Narkotika <?php print_r($str_kesimpulan_obat);?>
				<br>Demikian Surat Keterangan Pemeriksaan Narkotika ini dibuat guna keperluan <?php print_r(ucwords($keperluan));?>
			</td>
		</tr>
	</table>


	<?php
		// print_r($_SESSION);
		$nama_petugas = "";
		// if($_SESSION["admin_lv_1"]){
		// 	$nama_petugas = $_SESSION["admin_lv_1"]["nama"];
		// }

		$nama_kepala = "";
		if($pejabat){
			$nama_kepala = $pejabat["nama_pejabat"];
		}


		$mengetahui_nama = "";
		$mengetahui_nip = "";

		if($array_mengetahui){
			$mengetahui_nama = $array_mengetahui["nama_pejabat"];
			$mengetahui_nip = $array_mengetahui["nip_pejabat"];
		}

		$nama_pelayan = "";
		$nip_pelayan = "";

		if($petugas_palayanan){
			$nama_pelayan = $petugas_palayanan["nama"];
			$nip_pelayan = $petugas_palayanan["nip"];
		}

		$str_menegetahui = "Kepala BNN Kabupaten Blitar";
		if($mengetahui == "1"){
			$str_menegetahui = "PLH Kepala BNN Kabupaten Blitar";
		}

	?>

	<br><br><br><br>

	<!-- <span style="font-family:Arial">This text is in Arial</span> -->
	<table border="0" width="100%" class="font_arial">
		<tr>
			<td align="center">
				<table border="0" width="100%">
					<tr>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td align="center"><b>Petugas Pemeriksa Urin</b></td>
					</tr>
					<tr>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td align="center"><b>( <?php print_r($nama_pelayan);?> )</b></td>
					</tr>
					<tr>
						<td align="center"><b>NIP: <?php print_r($nip_pelayan);?></b></td>
					</tr>
				</table>
			</td>
			<td align="center" width="50%">
				<table border="0" width="100%">
					<tr>
						<td align="center">Blitar, <?php print_r($str_tgl_daftar);?></td>
					</tr>
					<tr>
						<td align="center"><b>Dokter Pemeriksa</b></td>
					</tr>
					<tr>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td align="center"><b>( <?php print_r($nama_pejabat);?> )</b></td>
					</tr>
					<tr>
						<td align="center"><b>NIP: <?php print_r($nip_pejabat);?></b></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" colspan="2">
				<table border="0" width="100%">
					<tr>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td align="center">&nbsp;</td>
					<!-- </tr> -->
					<tr>
						<td align="center"><b>Mengetahui,</b></td>
					</tr>
					<tr>
						<td align="center"><b><?= $str_menegetahui; ?></b></td>
					</tr>
					<tr>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td align="center"><b>( <?php print_r($mengetahui_nama);?> )</b></td>
					</tr>
					<tr>
						<td align="center"><b>NIP: <?php print_r($mengetahui_nip);?></b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<script type="text/javascript">
	window.print(); 
	</script>
</body>
</html>


