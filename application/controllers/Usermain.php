<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usermain extends CI_Controller {

    public function __construct(){
        parent::__construct();  
        $this->load->model('admin_main', 'am');
        $this->load->model('main/mainmodel', 'mm');

        $this->load->library("response_message");
        
        $session = $this->session->userdata("admin_lv_1");

        if(isset($session)){
            if($session["status_active"] == "0" and $session["is_log"] == "1"){
                redirect(base_url("user/home"));
            }
        }
    }
    

#=============================================================================#
#-------------------------------------------Index_Admin-----------------------#
#=============================================================================#
    public function index_home(){
        $data["page"] = "page_home_user";

        $this->load->view("index_user", $data);
    }

    public function val_form(){
        $config_val_input = array(
                array(
                    'field'=>'email',
                    'label'=>'Email',
                    'rules'=>'required|valid_emails|is_unique[admin.email]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_emails'=>"%s ".$this->response_message->get_error_msg("EMAIL"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("EMAIL_AVAIL")
                    )  
                ),array(
                    'field'=>'username',
                    'label'=>'username',
                    'rules'=>'required|is_unique[admin.username]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("EMAIL_AVAIL")
                    ) 
                ),array(
                    'field'=>'nama',
                    'label'=>'Nama',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'id_lv',
                    'label'=>'Admin lv',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'pass',
                    'label'=>'Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )     
                ),array(
                    'field'=>'repass',
                    'label'=>'Ulangi Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_admin(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "email"=>"",
                    "username"=>"",
                    "nama"=>"",
                    "id_lv"=>"",
                    "pass"=>"",
                    "repass"=>""
                );

        if($this->val_form()){
            $nama = $this->input->post("nama");
            $email = $this->input->post("email");
            $username = $this->input->post("username");
            $id_lv = $this->input->post("id_lv");
            $pass = $this->input->post("pass");
            $repass = $this->input->post("repass");

            $admin_del = $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update = date("Y-m-d h:i:s");

            if ($pass == $repass) {
                // print_r($_POST);
                $data_send = array(
                                "id_admin"=>"",
                                "email"=>$email,
                                "username"=>$username,
                                "password"=>$pass,
                                "id_lv"=>$id_lv,
                                "status_active"=>"0",
                                "nama"=>$nama,
                                "is_delete"=>"0",
                                "admin_del"=>"0",
                                "time_update"=>$time_update
                            );
                $insert = $this->am->insert_admin($data_send);

                if($insert){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }else{
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("RE_PASSWORD_FAIL"));
                
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                            "email"=>strip_tags(form_error('email')),
                            "nama"=>strip_tags(form_error('nama')),
                            "username"=>strip_tags(form_error('username')),
                            "id_lv"=>strip_tags(form_error('id_lv')),
                            "pass"=>strip_tags(form_error('pass')),
                            "repass"=>strip_tags(form_error('repass'))
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function get_admin_update(){
        $id = $this->input->post("id_admin");
        $data = $this->mm->get_data_each("admin", array("id_admin"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
            // $data_json = ;
        }

        print_r(json_encode($data_json));
    }
    
    public function val_form_update(){
        $config_val_input = array(
                array(
                    'field'=>'email',
                    'label'=>'Email',
                    'rules'=>'required|valid_emails',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_emails'=>"%s ".$this->response_message->get_error_msg("EMAIL")
                    )
                       
                ),
                array(
                    'field'=>'username',
                    'label'=>'username',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'nama',
                    'label'=>'Nama',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'id_lv',
                    'label'=>'Admin lv',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update_admin(){
        // print_r($_POST);
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array(
                    "email"=>"",
                    "nama"=>"",
                    "username"=>"",
                    "id_lv"=>""
                );

        if($this->val_form_update()){
            $id_admin = $this->input->post("id_admin");

            $nama = $this->input->post("nama");
            $email = $this->input->post("email");
            $username = $this->input->post("username");

            $id_lv = $this->input->post("id_lv");

            $admin_del = $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update = date("Y-m-d h:i:s");

            if($this->mm->get_data_each("admin", array("email"=>$email, "id_admin!="=>$id_admin))){
                $msg_detail["email"] = "email sudah terdaftar, silahkan gunakan email yang belum terdaftar";
            }else{
                $set = array(
                        "nama"=>$nama,
                        "email"=>$email,
                        "id_lv"=>$id_lv,
                        "username"=>$username
                    );

                $where = array(
                            "id_admin"=>$id_admin
                        );

                $update = $this->mm->update_data("admin", $set, $where);
                if($update){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }
            }            
        }else{
            $msg_detail["email"] = strip_tags(form_error('email'));
            $msg_detail["nama"] = strip_tags(form_error('nama'));
            $msg_detail["username"] = strip_tags(form_error('username'));
            $msg_detail["id_lv"] = strip_tags(form_error('id_lv'));
                            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function val_form_delete(){
        $config_val_input = array(
                array(
                    'field'=>'id_admin',
                    'label'=>'id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_admin(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        if($this->val_form_delete()){
            $id_admin = $this->input->post("id_admin");

            $is_del = "1";
            $time_del = date("Y-m-d h:i:s");

            $set = array(
                    "is_delete"=>$is_del,
                    "time_update"=>$time_del
                );

            $where = array("id_admin"=>$id_admin);

            if($this->mm->update_data("admin", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }



    public function activate_admin(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("ACTIVATION_FAIL"));
        if($this->val_form_delete()){
            $id_admin = $this->input->post("id_admin");

            $sts_active = "1";
            $time_del = date("Y-m-d h:i:s");

            $set = array(
                    "status_active"=>$sts_active,
                    "time_update"=>$time_del
                );

            $where = array("id_admin"=>$id_admin);

            if($this->mm->update_data("admin", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("ACTIVATION_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }

    public function unactivate_admin(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("ACTIVATION_FAIL"));
        if($this->val_form_delete()){
            $id_admin = $this->input->post("id_admin");

            $sts_active = "0";
            $time_del = date("Y-m-d h:i:s");

            $set = array(
                    "status_active"=>$sts_active,
                    "time_update"=>$time_del
                );

            $where = array("id_admin"=>$id_admin);

            if($this->mm->update_data("admin", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("ACTIVATION_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }
#=============================================================================#
#-------------------------------------------Index_Admin-----------------------#
#=============================================================================#
}
