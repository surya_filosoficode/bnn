<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Periksa extends CI_Controller {

    public function __construct(){
        parent::__construct();  
        $this->load->model('admin_main', 'am');
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('mainmaster', 'ms');

        $this->load->library("response_message");
        
        $session = $this->session->userdata("admin_lv_1");

        // $this->encrypt->set_cipher(MCRYPT_BLOWFISH);
        if(isset($session)){
            if($session["status_active"] != "0" and $session["is_log"] != "1"){
                redirect(base_url("login"));
            }
        }
    }

#=================================================================================================#
#-------------------------------------------main_upload_file--------------------------------------#
#=================================================================================================#
    private function main_upload_file($config, $input_name){
        if(file_exists($config['upload_path'].$config['file_name'])){
            unlink($config['upload_path'].$config['file_name']);    
        }

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        $return_array = array("status"=>"",
                                "main_msg"=>"",
                                "main_data"=>"");
        
        if (!$this->upload->do_upload($input_name)){
            $return_array["status"] = false;
            $return_array["main_msg"] = array('error' => $this->upload->display_errors());
            $return_array["main_data"] = null;
        }else{
            $return_array["status"] = true;
            $return_array["main_msg"] = "upload success";
            $return_array["main_data"] = array('upload_data' => $this->upload->data());
        }

        // print_r($return_array);

        return $return_array;
    }
#=================================================================================================#
#-------------------------------------------main_upload_file--------------------------------------#
#=================================================================================================#

    

#=============================================================================#
#-------------------------------------------Index_periksa---------------------#
#=============================================================================#

    public function index_periksa(){
        $data["page"] = "periksa";
        $data["data_kab"] = $this->mm->get_data_all("db_kab");
        $data["data_kec"] = $this->mm->get_data_all("db_kec");
        // $data["data_kel"] = $this->ms->get_kel_all();

        $array_new = array();
        foreach ($data["data_kec"] as $key => $value) {
            $array_new[$value->id_kec] = $this->ms->get_kel_where(array("kl.id_kec"=>$value->id_kec));
        }

        // print_r($array_new);
        $data["data_kel"] = json_encode($array_new);

        $data["data_list"] = $this->ms->get_periksa_all_where(array());

        // print_r($data["data_list"]);
        $this->load->view('index', $data);
    }


    public function val_form_periksa(){
        $config_val_input = array(
                array(
                    'field'=>'jenis_identitas',
                    'label'=>'jenis_identitas',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'no_identitas',
                    'label'=>'no_identitas',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'no_hp',
                    'label'=>'no_hp',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'nama',
                    'label'=>'nama',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'alamat',
                    'label'=>'alamat',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'jk',
                    'label'=>'jk',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'tmp_lhr',
                    'label'=>'tmp_lhr',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'tgl_lhr',
                    'label'=>'tgl_lhr',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'kel',
                    'label'=>'kel',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'kec',
                    'label'=>'kec',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'kab',
                    'label'=>'kab',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'pekerjaan',
                    'label'=>'pekerjaan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_periksa(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "jenis_identitas"=>"",
                    "no_identitas"=>"",
                    "foto"=>"",
                    "no_hp"=>"",
                    "nama"=>"",
                    "alamat"=>"",
                    "jk"=>"",
                    "tmp_lhr"=>"",
                    "tgl_lhr"=>"",
                    "kel"=>"",
                    "kec"=>"",
                    "kab"=>"",
                    "pekerjaan"=>""
                );

        if($this->val_form_periksa()){
            // print_r($_POST);
            // print_r($_FILES);
            $tgl_daftar = date("y-m-d H:i:s");
            
            $jenis_identitas = $this->input->post("jenis_identitas");
            $no_identitas = $this->input->post("no_identitas");
            // $foto = $this->input->post("foto");
            $no_hp = $this->input->post("no_hp");
            $nama = $this->input->post("nama");
            $alamat = $this->input->post("alamat");
            $jk = $this->input->post("jk");
            $tmp_lhr = $this->input->post("tmp_lhr");
            $tgl_lhr = $this->input->post("tgl_lhr");
            $kel = $this->input->post("kel");
            $kec = $this->input->post("kec");
            $kab = $this->input->post("kab");
            $pekerjaan = $this->input->post("pekerjaan");

            $id_pasien = "";
            $str_foto = "";

            $config['upload_path']          = './assets/doc/pasien/';
            $config['allowed_types']        = "jpg|png";
            $config['max_size']             = 2048;
            $config['file_name']            = date("YmdHis").".jpg";
               
            $upload_data = $this->main_upload_file($config, "foto");

            // print_r($upload_data);
            
            if($upload_data["status"]){
                // $set["foto"] = $upload_data["main_data"]["upload_data"]["file_name"];

                $str_foto = $upload_data["main_data"]["upload_data"]["file_name"];
                $data_send = array(
                            "id_data"=>"",
                            "id_pasien"=>$id_pasien,
                            "tgl_daftar"=>$tgl_daftar,
                            "jenis_identitas"=>$jenis_identitas,
                            "no_identitas"=>$no_identitas,
                            "foto"=>$str_foto,
                            "no_hp"=>$no_hp,
                            "nama"=>$nama,
                            "alamat"=>$alamat,
                            "jk"=>$jk,
                            "tmp_lhr"=>$tmp_lhr,
                            "tgl_lhr"=>$tgl_lhr,
                            "kel"=>$kel,
                            "kec"=>$kec,
                            "kab"=>$kab,
                            "pekerjaan"=>$pekerjaan,
                            "sts_periksa"=>"0"
                        );
                $insert = $this->mm->insert_data("data_pasien_new", $data_send);

                if($insert){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }else{
                // print_r($upload_data);
                $msg_detail["foto"] = $upload_data["main_msg"]["error"];
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
            }
              
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                            "jenis_identitas"=>strip_tags(form_error('jenis_identitas')),
                            "no_identitas"=>strip_tags(form_error('no_identitas')),
                            "no_hp"=>strip_tags(form_error('no_hp')),
                            "nama"=>strip_tags(form_error('nama')),
                            "alamat"=>strip_tags(form_error('alamat')),
                            "jk"=>strip_tags(form_error('jk')),
                            "tmp_lhr"=>strip_tags(form_error('tmp_lhr')),
                            "tgl_lhr"=>strip_tags(form_error('tgl_lhr')),
                            "kel"=>strip_tags(form_error('kel')),
                            "kec"=>strip_tags(form_error('kec')),
                            "kab"=>strip_tags(form_error('kab')),
                            "pekerjaan"=>strip_tags(form_error('pekerjaan'))

                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function get_periksa_update(){
        $id = $this->input->post("id_data");
        $data = $this->mm->get_data_each("data_pasien_new", array("id_data"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
            $data_json["url_base"] = base_url()."assets/doc/pasien/";
            // $data_json = ;
        }

        print_r(json_encode($data_json));
    }



    public function update_periksa(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "jenis_identitas"=>"",
                    "no_identitas"=>"",
                    "foto"=>"",
                    "no_hp"=>"",
                    "nama"=>"",
                    "alamat"=>"",
                    "jk"=>"",
                    "tmp_lhr"=>"",
                    "tgl_lhr"=>"",
                    "kel"=>"",
                    "kec"=>"",
                    "kab"=>"",
                    "pekerjaan"=>""
                );

        if($this->val_form_periksa()){
            // print_r($_POST);
            // print_r($_FILES);
            $tgl_daftar = date("y-m-d H:i:s");
            $jenis_identitas = $this->input->post("jenis_identitas");
            $no_identitas = $this->input->post("no_identitas");
            // $foto = $this->input->post("foto");
            $no_hp = $this->input->post("no_hp");
            $nama = $this->input->post("nama");
            $alamat = $this->input->post("alamat");
            $jk = $this->input->post("jk");
            $tmp_lhr = $this->input->post("tmp_lhr");
            $tgl_lhr = $this->input->post("tgl_lhr");
            $kel = $this->input->post("kel");
            $kec = $this->input->post("kec");
            $kab = $this->input->post("kab");
            $pekerjaan = $this->input->post("pekerjaan");

            $id_data = $this->input->post("id_data");

            $id_pasien = "";
            $str_foto = "";

            $data_send = array(
                "id_pasien"=>$id_pasien,
                "tgl_daftar"=>$tgl_daftar,
                "jenis_identitas"=>$jenis_identitas,
                "no_identitas"=>$no_identitas,
                "no_hp"=>$no_hp,
                "nama"=>$nama,
                "alamat"=>$alamat,
                "jk"=>$jk,
                "tmp_lhr"=>$tmp_lhr,
                "tgl_lhr"=>$tgl_lhr,
                "kel"=>$kel,
                "kec"=>$kec,
                "kab"=>$kab,
                "pekerjaan"=>$pekerjaan
            );

            $where_send = array("id_data"=>$id_data);

            $config['upload_path']          = './assets/doc/pasien/';
            $config['allowed_types']        = "jpg|png";
            $config['max_size']             = 2048;
            $config['file_name']            = date("YmdHis").".jpg";
               
            $upload_data = $this->main_upload_file($config, "foto");

            // print_r($upload_data);
            
            if($upload_data["status"]){
                $data_send["foto"] = $upload_data["main_data"]["upload_data"]["file_name"];

                $str_foto = $upload_data["main_data"]["upload_data"]["file_name"];
            }else{
                // print_r($upload_data);
                $msg_detail["foto"] = $upload_data["main_msg"]["error"];
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
            }

            $insert = $this->mm->update_data("data_pasien_new", $data_send, $where_send);

            if($insert){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }
              
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                            "jenis_identitas"=>strip_tags(form_error('jenis_identitas')),
                            "no_identitas"=>strip_tags(form_error('no_identitas')),
                            "no_hp"=>strip_tags(form_error('no_hp')),
                            "nama"=>strip_tags(form_error('nama')),
                            "alamat"=>strip_tags(form_error('alamat')),
                            "jk"=>strip_tags(form_error('jk')),
                            "tmp_lhr"=>strip_tags(form_error('tmp_lhr')),
                            "tgl_lhr"=>strip_tags(form_error('tgl_lhr')),
                            "kel"=>strip_tags(form_error('kel')),
                            "kec"=>strip_tags(form_error('kec')),
                            "kab"=>strip_tags(form_error('kab')),
                            "pekerjaan"=>strip_tags(form_error('pekerjaan'))

                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }


    public function val_form_del_periksa(){
        $config_val_input = array(
                array(
                    'field'=>'id_data',
                    'label'=>'id_data',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_periksa(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_data"=>""
                );

        if($this->val_form_del_periksa()){
            $id_data = $this->input->post("id_data");
            
            $where_send = array(
                                "id_data"=>$id_data
                            );

            $insert = $this->mm->delete_data("data_pasien_new", $where_send);

            if($insert){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
              
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                            "id_data"=>strip_tags(form_error('id_data')),
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#=============================================================================#
#-------------------------------------------Index_periksa-------------------#
#=============================================================================#


#=============================================================================#
#---------------------------------Index_periksa_for_admin---------------------#
#=============================================================================#
    public function index_periksa_admin(){
        $data["page"] = "periksa_admin";
        $data["data_kab"] = $this->mm->get_data_all("db_kab");
        $data["data_kec"] = $this->mm->get_data_all("db_kec");

        $array_new = array();
        foreach ($data["data_kec"] as $key => $value) {
            $array_new[$value->id_kec] = $this->ms->get_kel_where(array("kl.id_kec"=>$value->id_kec));
        }

        $data["data_kel"] = json_encode($array_new);

        $data["data_list"] = $this->ms->get_periksa_all_where(array());

        $this->load->view('index', $data);
    }
#=============================================================================#
#---------------------------------Index_periksa_for_admin---------------------#
#=============================================================================#

}
