<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemeriksaan extends CI_Controller {

    public function __construct(){
        parent::__construct();  
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('mainmaster', 'ms');

        $this->load->library("response_message");
        
        $session = $this->session->userdata("admin_lv_1");

        // $this->encrypt->set_cipher(MCRYPT_BLOWFISH);
        if(isset($session)){
            if($session["status_active"] != "0" and $session["is_log"] != "1"){
                redirect(base_url("login"));
            }
        }
    }
    

#=============================================================================#
#-------------------------------------------Index_periksa---------------------#
#=============================================================================#

    public function index_pemeriksaan(){
        $data["page"] = "pemeriksaan";
        $data["pasien"] = $this->ms->get_periksa_all_where(array("sts_periksa"=>"0"));
        $data["data_list"] = $this->ms->get_pemeriksaan_all_where(array());

        $data["dokter"] = $this->mm->get_data_all_where("tbl_pejabat", array("tipe_pejabat"=>"1"));

        $this->load->view('index', $data);
    }

    public function index_pemeriksaan_all($id = null){

        $id_admin = "";
        if(isset($_SESSION["admin_lv_1"])){
            $id_admin = $_SESSION["admin_lv_1"]["id_admin"];
        }
        $data["page"] = "pemeriksaan";
        $data["pasien"] = $this->ms->get_periksa_all_where(array("sts_periksa"=>"0"));
        $data["data_list"] = $this->ms->get_pemeriksaan_all_where(array("tp.id_pasien"=>$id, "tp.id_admin_layanan"=>$id_admin));
        
        $data["pasien_in"] = json_encode($this->ms->get_periksa_all_where(array("id_data"=>$id)));

        $data["dokter"] = $this->mm->get_data_all_where("tbl_pejabat", array("tipe_pejabat"=>"1"));

        $this->load->view('index', $data);
    }

    public function index_pemeriksaan_by_user_for_admin($id = null){

        $id_admin = "";
        if(isset($_SESSION["admin_lv_1"])){
            $id_admin = $_SESSION["admin_lv_1"]["id_admin"];
        }
        $data["page"] = "pemeriksaan_admin";
        $data["pasien"] = $this->ms->get_periksa_all_where(array("sts_periksa"=>"0"));
        $data["data_list"] = $this->ms->get_pemeriksaan_all_where(array("tp.id_pasien"=>$id));
        
        $data["pasien_in"] = json_encode($this->ms->get_periksa_all_where(array("id_data"=>$id)));

        $data["dokter"] = $this->mm->get_data_all_where("tbl_pejabat", array("tipe_pejabat"=>"1"));

        $this->load->view('index', $data);
    }

    public function index_pemeriksaan_all_for_admin(){

        $id_admin = "";
        if(isset($_SESSION["admin_lv_1"])){
            $id_admin = $_SESSION["admin_lv_1"]["id_admin"];
        }
        $data["page"] = "pemeriksaan_admin_full";
        $data["pasien"] = $this->ms->get_periksa_all_where(array("sts_periksa"=>"0"));
        $data["data_list"] = $this->ms->get_pemeriksaan_all_where(array("tp.status != "=> "0"));
        
        $data["pasien_in"] = json_encode($this->ms->get_periksa_all_where(array()));

        $data["dokter"] = $this->mm->get_data_all_where("tbl_pejabat", array("tipe_pejabat"=>"1"));

        $this->load->view('index', $data);

        // print_r($data);
    }

    public function get_data_iden($id_data){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array();
        $data = $this->ms->get_iden_all_where_xx(array("id_data"=>$id_data));
        if($data){
            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
            $msg_detail["item"] = $data;
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }


    public function val_form_pemeriksaan(){
        $config_val_input = array(
                array(
                    'field'=>'bertempat_dari',
                    'label'=>'bertempat_dari',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'permintaan_dari',
                    'label'=>'permintaan_dari',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'srt_pengantar',
                    'label'=>'srt_pengantar',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                // array(
                //     'field'=>'nomer_surat',
                //     'label'=>'nomer_surat',
                //     'rules'=>'required',
                //     'errors'=>array(
                //         'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                //     )
                       
                // ),array(
                //     'field'=>'status',
                //     'label'=>'status',
                //     'rules'=>'required',
                //     'errors'=>array(
                //         'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                //     )
                       
                // ),
                array(
                    'field'=>'kesadaran',
                    'label'=>'kesadaran',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'keumum',
                    'label'=>'keumum',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'tekanandarah',
                    'label'=>'tekanandarah',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'nadi',
                    'label'=>'nadi',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'pernafasan',
                    'label'=>'pernafasan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'penggunaanobat',
                    'label'=>'penggunaanobat',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'jenisobat',
                    'label'=>'jenisobat',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'asalobat',
                    'label'=>'asalobat',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'terakirminum',
                    'label'=>'terakirminum',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'hasiltes',
                    'label'=>'hasiltes',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'metode',
                    'label'=>'metode',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'amphe',
                    'label'=>'amphe',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'metha',
                    'label'=>'metha',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'coco',
                    'label'=>'coco',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'opioid',
                    'label'=>'opioid',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'thc',
                    'label'=>'thc',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'benzo',
                    'label'=>'benzo',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'k2',
                    'label'=>'k2',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'keperluan',
                    'label'=>'keperluan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'pemeriksa',
                    'label'=>'pemeriksa',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_pemeriksaan(){
        date_default_timezone_set("Asia/Bangkok");
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_pasien"=>"",
                    "bertempat_dari"=>"",
                    "permintaan_dari"=>"",
                    "srt_pengantar"=>"",
                    // "nomer_surat"=>"",
                    // "status"=>"",
                    "kesadaran"=>"",
                    "keumum"=>"",
                    "tekanandarah"=>"",
                    "nadi"=>"",
                    "pernafasan"=>"",
                    "penggunaanobat"=>"",
                    "jenisobat"=>"",
                    "asalobat"=>"",
                    "terakirminum"=>"",
                    
                    "hasiltes"=>"",
                    "metode"=>"",
                    "amphe"=>"",
                    "metha"=>"",
                    "coco"=>"",
                    "opioid"=>"",
                    "thc"=>"",
                    "benzo"=>"",
                    "k2"=>"",
                    "lain"=>"",
                    "keperluan"=>"",
                    "pemeriksa"=>""
                );

        if($this->val_form_pemeriksaan()){
            // print_r($_POST);
            // print_r($_FILES);
            $id_pasien = $this->input->post("id_pasien");
            $bertempat_dari = $this->input->post("bertempat_dari");
            $permintaan_dari = $this->input->post("permintaan_dari");
            $srt_pengantar = $this->input->post("srt_pengantar");
            // $nomer_surat = $this->input->post("nomer_surat");
            // $foto = $this->input->post("foto");
            // $status = $this->input->post("status");
            $kesadaran = $this->input->post("kesadaran");
            $keumum = $this->input->post("keumum");
            $tekanandarah = $this->input->post("tekanandarah");
            $nadi = $this->input->post("nadi");
            $pernafasan = $this->input->post("pernafasan");
            $penggunaanobat = $this->input->post("penggunaanobat");
            $jenisobat = $this->input->post("jenisobat");
            $asalobat = $this->input->post("asalobat");
            $terakirminum = $this->input->post("terakirminum");

            $hasiltes = $this->input->post("hasiltes");
            $metode = $this->input->post("metode");
            $amphe = $this->input->post("amphe");
            $metha = $this->input->post("metha");
            $coco = $this->input->post("coco");
            $opioid = $this->input->post("opioid");
            $metode = $this->input->post("metode");
            $thc = $this->input->post("thc");
            $benzo = $this->input->post("benzo");
            $k2 = $this->input->post("k2");
            $lain = $this->input->post("lain");
            $keperluan = $this->input->post("keperluan");
            $pemeriksa = $this->input->post("pemeriksa");
            $admin_pelayanan = $this->input->post("admin_pelayanan");
            $mengetahui = $this->input->post("mengetahui");

            $data_send = array(
                            "id"=>"",
                            "waktu"=>date("Y-m-d H:i:s"),
                            "id_pasien"=>$id_pasien,
                            "bertempat_dari"=>$bertempat_dari,
                            "permintaan_dari"=>$permintaan_dari,
                            "srt_pengantar"=>$srt_pengantar,
                            "nomer_surat"=>"0",
                            "status"=>"0",
                            "kesadaran"=>$kesadaran,
                            "keumum"=>$keumum,
                            "tekanandarah"=>$tekanandarah,
                            "nadi"=>$nadi,
                            "pernafasan"=>$pernafasan,
                            "penggunaanobat"=>$penggunaanobat,
                            "jenisobat"=>$jenisobat,
                            "asalobat"=>$asalobat,
                            "terakirminum"=>$terakirminum,
                            "hasiltes"=>$hasiltes,

                            "metode"=>$metode,
                            "amphe"=>$amphe,
                            "metha"=>$metha,
                            "coco"=>$coco,
                            "opioid"=>$opioid,
                            "thc"=>$thc,
                            "benzo"=>$benzo,
                            "k2"=>$k2,
                            "lain"=>$lain,
                            "keperluan"=>$keperluan,
                            "id_pejabat"=>$pemeriksa,
                            "id_admin_layanan"=>$admin_pelayanan,
                            "mengetahui"=>$mengetahui
                        );            

            if(!$this->mm->get_data_all_where("tbl_periksa", array("id_pasien"=>$id_pasien,
                "status"=>"0"))){
                $insert = $this->mm->insert_data("tbl_periksa", $data_send);
                if($insert){
                    if($this->mm->update_data("data_pasien_new", array("sts_periksa"=>"1"), array("id_data"=>$id_pasien))){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                    }
                }
            }
              
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                            "id_pasien"=>strip_tags(form_error('id_pasien')),
                            "bertempat_dari"=>strip_tags(form_error('bertempat_dari')),
                            "permintaan_dari"=>strip_tags(form_error('permintaan_dari')),
                            "srt_pengantar"=>strip_tags(form_error('srt_pengantar')),
                            // "nomer_surat"=>strip_tags(form_error('nomer_surat')),
                            // "status"=>strip_tags(form_error('status')),
                            "kesadaran"=>strip_tags(form_error('kesadaran')),
                            "keumum"=>strip_tags(form_error('keumum')),
                            "tekanandarah"=>strip_tags(form_error('tekanandarah')),
                            "nadi"=>strip_tags(form_error('nadi')),
                            "pernafasan"=>strip_tags(form_error('pernafasan')),
                            "penggunaanobat"=>strip_tags(form_error('penggunaanobat')),
                            "jenisobat"=>strip_tags(form_error('jenisobat')),
                            "asalobat"=>strip_tags(form_error('asalobat')),
                            "terakirminum"=>strip_tags(form_error('terakirminum')),
                            "hasiltes"=>strip_tags(form_error('hasiltes')),
                            "metode"=>strip_tags(form_error('metode')),
                            "amphe"=>strip_tags(form_error('amphe')),
                            "metha"=>strip_tags(form_error('metha')),
                            "coco"=>strip_tags(form_error('coco')),
                            "opioid"=>strip_tags(form_error('opioid')),
                            "thc"=>strip_tags(form_error('thc')),
                            "benzo"=>strip_tags(form_error('benzo')),
                            "k2"=>strip_tags(form_error('k2')),
                            "lain"=>strip_tags(form_error('lain')),
                            "keperluan"=>strip_tags(form_error('keperluan')),
                            "pemeriksa"=>strip_tags(form_error('pemeriksa'))

                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function get_pemeriksaan_update($id){
        $data = $this->ms->get_pemeriksaan_all_where_xx(array("id"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
            $data_json["url_base"] = base_url()."assets/doc/pasien/";
            // $data_json = ;
        }

        print_r(json_encode($data_json));
    }



    public function update_pemeriksaan(){
        date_default_timezone_set("Asia/Bangkok");
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_pasien"=>"",
                    "bertempat_dari"=>"",
                    "permintaan_dari"=>"",
                    "srt_pengantar"=>"",
                    // "nomer_surat"=>"",
                    // "status"=>"",
                    "kesadaran"=>"",
                    "keumum"=>"",
                    "tekanandarah"=>"",
                    "nadi"=>"",
                    "pernafasan"=>"",
                    "penggunaanobat"=>"",
                    "jenisobat"=>"",
                    "asalobat"=>"",
                    "terakirminum"=>"",
                    
                    "hasiltes"=>"",
                    "metode"=>"",
                    "amphe"=>"",
                    "metha"=>"",
                    "coco"=>"",
                    "opioid"=>"",
                    "thc"=>"",
                    "benzo"=>"",
                    "k2"=>"",
                    "lain"=>"",
                    "keperluan"=>"",
                    "pemeriksa"=>""
                );

        if($this->val_form_pemeriksaan()){
            // print_r($_POST);
            // print_r($_FILES);
            // $id_pasien = $this->input->post("id_pasien");
            $bertempat_dari = $this->input->post("bertempat_dari");
            $permintaan_dari = $this->input->post("permintaan_dari");
            $srt_pengantar = $this->input->post("srt_pengantar");
            // $nomer_surat = $this->input->post("nomer_surat");
            // $foto = $this->input->post("foto");
            // $status = $this->input->post("status");
            $kesadaran = $this->input->post("kesadaran");
            $keumum = $this->input->post("keumum");
            $tekanandarah = $this->input->post("tekanandarah");
            $nadi = $this->input->post("nadi");
            $pernafasan = $this->input->post("pernafasan");
            $penggunaanobat = $this->input->post("penggunaanobat");
            $jenisobat = $this->input->post("jenisobat");
            $asalobat = $this->input->post("asalobat");
            $terakirminum = $this->input->post("terakirminum");

            $hasiltes = $this->input->post("hasiltes");
            $metode = $this->input->post("metode");
            $amphe = $this->input->post("amphe");
            $metha = $this->input->post("metha");
            $coco = $this->input->post("coco");
            $opioid = $this->input->post("opioid");
            $metode = $this->input->post("metode");
            $thc = $this->input->post("thc");
            $benzo = $this->input->post("benzo");
            $k2 = $this->input->post("k2");
            $lain = $this->input->post("lain");
            $keperluan = $this->input->post("keperluan");

            $pemeriksa = $this->input->post("pemeriksa");
            $admin_pelayanan = $this->input->post("admin_pelayanan");
            $mengetahui = $this->input->post("mengetahui");

            $id = $this->input->post("id");

            $data_send = array(
                            "waktu"=>date("Y-m-d H:i:s"),
                            // "id_pasien"=>$id_pasien,
                            "bertempat_dari"=>$bertempat_dari,
                            "permintaan_dari"=>$permintaan_dari,
                            "srt_pengantar"=>$srt_pengantar,
                            // "nomer_surat"=>"0",
                            // "status"=>"0",
                            "kesadaran"=>$kesadaran,
                            "keumum"=>$keumum,
                            "tekanandarah"=>$tekanandarah,
                            "nadi"=>$nadi,
                            "pernafasan"=>$pernafasan,
                            "penggunaanobat"=>$penggunaanobat,
                            "jenisobat"=>$jenisobat,
                            "asalobat"=>$asalobat,
                            "terakirminum"=>$terakirminum,
                            "hasiltes"=>$hasiltes,

                            "metode"=>$metode,
                            "amphe"=>$amphe,
                            "metha"=>$metha,
                            "coco"=>$coco,
                            "opioid"=>$opioid,
                            "thc"=>$thc,
                            "benzo"=>$benzo,
                            "k2"=>$k2,
                            "lain"=>$lain,
                            "keperluan"=>$keperluan,
                            "id_pejabat"=>$pemeriksa,
                            "id_admin_layanan"=>$admin_pelayanan,
                            "mengetahui"=>$mengetahui
                        );

            $where_send = array("id"=>$id);
            $insert = $this->mm->update_data("tbl_periksa", $data_send, $where_send);

            if($insert){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }
            
              
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                            "id_pasien"=>strip_tags(form_error('id_pasien')),
                            "bertempat_dari"=>strip_tags(form_error('bertempat_dari')),
                            "permintaan_dari"=>strip_tags(form_error('permintaan_dari')),
                            "srt_pengantar"=>strip_tags(form_error('srt_pengantar')),
                            // "nomer_surat"=>strip_tags(form_error('nomer_surat')),
                            // "status"=>strip_tags(form_error('status')),
                            "kesadaran"=>strip_tags(form_error('kesadaran')),
                            "keumum"=>strip_tags(form_error('keumum')),
                            "tekanandarah"=>strip_tags(form_error('tekanandarah')),
                            "nadi"=>strip_tags(form_error('nadi')),
                            "pernafasan"=>strip_tags(form_error('pernafasan')),
                            "penggunaanobat"=>strip_tags(form_error('penggunaanobat')),
                            "jenisobat"=>strip_tags(form_error('jenisobat')),
                            "asalobat"=>strip_tags(form_error('asalobat')),
                            "terakirminum"=>strip_tags(form_error('terakirminum')),
                            "hasiltes"=>strip_tags(form_error('hasiltes')),
                            "metode"=>strip_tags(form_error('metode')),
                            "amphe"=>strip_tags(form_error('amphe')),
                            "metha"=>strip_tags(form_error('metha')),
                            "coco"=>strip_tags(form_error('coco')),
                            "opioid"=>strip_tags(form_error('opioid')),
                            "thc"=>strip_tags(form_error('thc')),
                            "benzo"=>strip_tags(form_error('benzo')),
                            "k2"=>strip_tags(form_error('k2')),
                            "lain"=>strip_tags(form_error('lain')),
                            "keperluan"=>strip_tags(form_error('keperluan')),
                            "pemeriksa"=>strip_tags(form_error('pemeriksa'))

                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }


    public function val_form_del_pemeriksaan(){
        $config_val_input = array(
                array(
                    'field'=>'id',
                    'label'=>'id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_pemeriksaan(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id"=>""
                );

        if($this->val_form_del_pemeriksaan()){
            $id = $this->input->post("id");
            $get_data = $this->mm->get_data_each("tbl_periksa", array("id"=>$id));
            
            if($get_data){
                if($this->mm->update_data("data_pasien_new", array("sts_periksa"=>"0"), array("id_data"=>$get_data["id_pasien"]))){
                    $where_send = array(
                                "id"=>$id
                            );

                    $insert = $this->mm->delete_data("tbl_periksa", $where_send);

                    if($insert){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
                    }
                }
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                            "id"=>strip_tags(form_error('id')),
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }


    public function get_vert(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id"=>""
                );
        $id = $this->input->post("id");

        $y_now = date("Y");
        $m_now = date("m");

        $vert = $this->db->query("select s_ket('".$id."', '".$y_now."', '".$m_now."') no_surat");

        if($vert){
            $msg_main = array("status"=>true, "msg"=>"berhasil di verifikasi");
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function get_srt($id_pemeriksaan = "0"){
        $data["detail"] = $this->ms->get_pemeriksaan_all_where_xx(array("id"=>$id_pemeriksaan));
        $data["pejabat"] = $this->mm->get_data_each("tbl_pejabat", array("tipe_pejabat"=>"2"));
        $data["data_month"] = array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
        $data["data_month_rom"] = array("","I","II","II","Iv","V","VI","VII","VIII","IX","X","XI","XII");

        $data["petugas_palayanan"] = $this->mm->get_data_each("admin", array("id_admin"=> $data["detail"]["id_admin_layanan"]));

        $data["mengetahui_data"][0] = $this->mm->get_data_each("tbl_pejabat", array("tipe_pejabat"=>"2"));
        $data["mengetahui_data"][1] = $this->mm->get_data_each("tbl_pejabat", array("tipe_pejabat"=>"3")); 
        // print_r($data);
        $this->load->view("admin/sk", $data);
    }
#=============================================================================#
#-------------------------------------------Index_periksa-------------------#
#=============================================================================#

}
