<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {

    public function __construct(){
        parent::__construct();  
        $this->load->model('admin_main', 'am');
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('mainmaster', 'ms');

        $this->load->library("response_message");
        
        $session = $this->session->userdata("admin_lv_1");

        // $this->encrypt->set_cipher(MCRYPT_BLOWFISH);
        if(isset($session)){
            if($session["status_active"] == "0" and $session["is_log"] == "1"){
                redirect(base_url("admin/home"));
            }
        }
    }

    
    public function index(){
        $data["page"] = "home";
        $data["count_pasien"] = count($this->mm->get_data_all_where("data_pasien_new", array()));

        $data["count_periksa_done"] = count($this->mm->get_data_all_where("tbl_periksa", array("status"=>"1")));

        $data["count_periksa_fail"] = count($this->mm->get_data_all_where("tbl_periksa", array("status"=>"0")));
        $this->load->view('index', $data);
    }

#=============================================================================#
#-------------------------------------------Index_Wilayah---------------------#
#=============================================================================#

    public function index_wilayah(){
        $data["page"] = "wilayah";
        $data["data_list"] = $this->mm->get_data_all("db_kab");
        $this->load->view('index', $data);
    }


    public function val_form(){
        $config_val_input = array(
                array(
                    'field'=>'wilayah',
                    'label'=>'wilayah',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_kab(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "wilayah"=>""
                );

        if($this->val_form()){
            $wilayah = $this->input->post("wilayah");

            $data_send = array(
                                "id_kab"=>"",
                                "kab"=>$wilayah
                            );
            $insert = $this->mm->insert_data("db_kab", $data_send);

            if($insert){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }
              
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                            "wilayah"=>strip_tags(form_error('wilayah')),
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function get_kab_update(){
        $id = $this->input->post("id_kab");
        $data = $this->mm->get_data_each("db_kab", array("id_kab"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
            // $data_json = ;
        }

        print_r(json_encode($data_json));
    }



    public function update_kab(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "wilayah"=>""
                );

        if($this->val_form()){
            $id_kab = $this->input->post("id_kab");
            $wilayah = $this->input->post("wilayah");

            $where_send = array(
                                "id_kab"=>$id_kab
                            );

            $data_send = array(
                                "kab"=>$wilayah
                            );
            $insert = $this->mm->update_data("db_kab", $data_send, $where_send);

            if($insert){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }
              
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                            "wilayah"=>strip_tags(form_error('wilayah')),
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }


    public function val_form_del_kab(){
        $config_val_input = array(
                array(
                    'field'=>'id_kab',
                    'label'=>'id_kab',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_kab(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_kab"=>""
                );

        if($this->val_form_del_kab()){
            $id_kab = $this->input->post("id_kab");
            
            $where_send = array(
                                "id_kab"=>$id_kab
                            );

            $insert = $this->mm->delete_data("db_kab", $where_send);

            if($insert){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
              
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                            "id_kab"=>strip_tags(form_error('id_kab')),
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#=============================================================================#
#-------------------------------------------Index_Wilayah---------------------#
#=============================================================================#


#=============================================================================#
#-------------------------------------------Index_kecamatan-------------------#
#=============================================================================#

    public function index_kecamatan(){
        $data["page"] = "kecamatan";
        $data["data_list"] = $this->mm->get_data_all("db_kec");
        $this->load->view('index', $data);
    }


    public function val_form_kec(){
        $config_val_input = array(
                array(
                    'field'=>'kecamatan',
                    'label'=>'kecamatan',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_kec(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "kecamatan"=>""
                );

        if($this->val_form_kec()){
            $kecamatan = $this->input->post("kecamatan");

            $data_send = array(
                                "id_kec"=>"",
                                "kecamatan"=>$kecamatan
                            );
            $insert = $this->mm->insert_data("db_kec", $data_send);

            if($insert){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }
              
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                            "kecamatan"=>strip_tags(form_error('kecamatan')),
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function get_kec_update(){
        $id = $this->input->post("id_kec");
        $data = $this->mm->get_data_each("db_kec", array("id_kec"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
            // $data_json = ;
        }

        print_r(json_encode($data_json));
    }



    public function update_kec(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "kecamatan"=>""
                );

        if($this->val_form_kec()){
            $id_kec = $this->input->post("id_kec");
            $kecamatan = $this->input->post("kecamatan");

            $where_send = array(
                                "id_kec"=>$id_kec
                            );

            $data_send = array(
                                "kecamatan"=>$kecamatan
                            );
            $insert = $this->mm->update_data("db_kec", $data_send, $where_send);

            if($insert){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }
              
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                            "kecamatan"=>strip_tags(form_error('kecamatan')),
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }


    public function val_form_del_kec(){
        $config_val_input = array(
                array(
                    'field'=>'id_kec',
                    'label'=>'id_kec',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_kec(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_kec"=>""
                );

        if($this->val_form_del_kec()){
            $id_kec = $this->input->post("id_kec");
            
            $where_send = array(
                                "id_kec"=>$id_kec
                            );

            $insert = $this->mm->delete_data("db_kec", $where_send);

            if($insert){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
              
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                            "id_kec"=>strip_tags(form_error('id_kec')),
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#=============================================================================#
#-------------------------------------------Index_kecamatan-------------------#
#=============================================================================#


#=============================================================================#
#-------------------------------------------Index_kelurahan-------------------#
#=============================================================================#

    public function index_kelurahan(){
        $data["page"] = "kelurahan";
        $data_all = array();
        $data_kecamatan = $this->mm->get_data_all("db_kec");
        foreach ($data_kecamatan as $key => $value) {
            $data_kel = $this->mm->get_data_all_where("db_kelurahan", array("id_kec"=>$value->id_kec));


            $data_all[$value->id_kec]["nama_kec"] = $value->kecamatan;
            $data_all[$value->id_kec]["item"] = $data_kel;
        }

        $data["data_all"] = $data_all;
        $data["data_list"] = $this->ms->get_kel_all();

        // print_r($data["data_list"]);
        $this->load->view('index', $data);
    }


    public function val_form_kel(){
        $config_val_input = array(
                array(
                    'field'=>'kecamatan',
                    'label'=>'kecamatan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'kelurahan',
                    'label'=>'kelurahan',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_kel(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "kecamatan"=>"",
                    "kelurahan"=>""
                );

        if($this->val_form_kel()){
            $kecamatan = $this->input->post("kecamatan");
            $kelurahan = $this->input->post("kelurahan");

            $data_send = array(
                                "id_kel"=>"",
                                "id_kec"=>$kecamatan,
                                "kelurahan"=>$kelurahan
                            );
            $insert = $this->mm->insert_data("db_kelurahan", $data_send);

            if($insert){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }
              
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                            "kecamatan"=>strip_tags(form_error('kecamatan')),
                            "kelurahan"=>strip_tags(form_error('kelurahan')),
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function get_kel_update(){
        $id = $this->input->post("id_kel");
        $data = $this->mm->get_data_each("db_kelurahan", array("id_kel"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
            // $data_json = ;
        }

        print_r(json_encode($data_json));
    }



    public function update_kel(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "kecamatan"=>"",
                    "kelurahan"=>""
                );

        if($this->val_form_kel()){
            $id_kel = $this->input->post("id_kel");
            $kecamatan = $this->input->post("kecamatan");
            $kelurahan = $this->input->post("kelurahan");


            $where_send = array(
                                "id_kel"=>$id_kel
                            );

            $data_send = array(
                                "id_kec"=>$kecamatan,
                                "kelurahan"=>$kelurahan
                            );
            $insert = $this->mm->update_data("db_kelurahan", $data_send, $where_send);

            if($insert){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }
              
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                            "kecamatan"=>strip_tags(form_error('kecamatan')),
                            "kelurahan"=>strip_tags(form_error('kelurahan')),
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }


    public function val_form_del_kel(){
        $config_val_input = array(
                array(
                    'field'=>'id_kel',
                    'label'=>'id_kel',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_kel(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_kel"=>""
                );

        if($this->val_form_del_kel()){
            $id_kel = $this->input->post("id_kel");
            
            $where_send = array(
                                "id_kel"=>$id_kel
                            );

            $insert = $this->mm->delete_data("db_kelurahan", $where_send);

            if($insert){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
              
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                            "id_kel"=>strip_tags(form_error('id_kel')),
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#=============================================================================#
#-------------------------------------------Index_kelurahan-------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Index_tdd-------------------------#
#=============================================================================#

    public function index_tdd(){
        $data["page"] = "pejabat";
        $data["data_list"] = $this->mm->get_data_all("tbl_pejabat");
        $this->load->view('index', $data);
    }


    public function val_form_tdd(){
        $config_val_input = array(
                array(
                    'field'=>'tipe_pejabat',
                    'label'=>'tipe_pejabat',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'nip_pejabat',
                    'label'=>'nip_pejabat',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'nama_pejabat',
                    'label'=>'nama_pejabat',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_tdd(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "tipe_pejabat"=>"",
                    "nip_pejabat"=>"",
                    "nama_pejabat"=>""
                );

        if($this->val_form_tdd()){
            $tipe_pejabat = $this->input->post("tipe_pejabat");
            $nip_pejabat = $this->input->post("nip_pejabat");
            $nama_pejabat = $this->input->post("nama_pejabat");

            $data_send = array(
                                "id_pejabat"=>"",
                                "tipe_pejabat"=>$tipe_pejabat,
                                "nip_pejabat"=>$nip_pejabat,
                                "nama_pejabat"=>$nama_pejabat
                            );
            $insert = $this->mm->insert_data("tbl_pejabat", $data_send);

            if($insert){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }
              
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                            "tipe_pejabat"=>strip_tags(form_error('tipe_pejabat')),
                            "nip_pejabat"=>strip_tags(form_error('nip_pejabat')),
                            "nama_pejabat"=>strip_tags(form_error('nama_pejabat')),
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function get_tdd_update(){
        $id_pejabat = $this->input->post("id_pejabat");
        $data = $this->mm->get_data_each("tbl_pejabat", array("id_pejabat"=>$id_pejabat));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
            // $data_json = ;
        }

        print_r(json_encode($data_json));
    }



    public function update_tdd(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "tipe_pejabat"=>"",
                    "nip_pejabat"=>"",
                    "nama_pejabat"=>""
                );

        if($this->val_form_tdd()){
            $tipe_pejabat = $this->input->post("tipe_pejabat");
            $nip_pejabat = $this->input->post("nip_pejabat");
            $nama_pejabat = $this->input->post("nama_pejabat");
            $id_pejabat = $this->input->post("id_pejabat");

            $data_send = array(
                                "tipe_pejabat"=>$tipe_pejabat,
                                "nip_pejabat"=>$nip_pejabat,
                                "nama_pejabat"=>$nama_pejabat
                            );

            $where_send = array(
                                "id_pejabat"=>$id_pejabat
                            );
            $insert = $this->mm->update_data("tbl_pejabat", $data_send, $where_send);

            if($insert){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }
              
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                            "tipe_pejabat"=>strip_tags(form_error('tipe_pejabat')),
                            "nip_pejabat"=>strip_tags(form_error('nip_pejabat')),
                            "nama_pejabat"=>strip_tags(form_error('nama_pejabat')),
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }


    public function val_form_del_tdd(){
        $config_val_input = array(
                array(
                    'field'=>'id_pejabat',
                    'label'=>'id_pejabat',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_tdd(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_pejabat"=>""
                );

        if($this->val_form_del_tdd()){
            $id_pejabat = $this->input->post("id_pejabat");
            
            $where_send = array(
                                "id_pejabat"=>$id_pejabat
                            );

            $insert = $this->mm->delete_data("tbl_pejabat", $where_send);

            if($insert){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
              
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                            "id_pejabat"=>strip_tags(form_error('id_pejabat')),
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#=============================================================================#
#-------------------------------------------Index_tdd-------------------------#
#=============================================================================#

}
