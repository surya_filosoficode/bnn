<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainmaster extends CI_Model{
    
    public function get_kel_all(){
        $this->db->join("db_kec kc", "kc.id_kec = kl.id_kec");
    	$data = $this->db->get("db_kelurahan kl");
    	return $data->result();
    }

    public function get_kel_where($where){
        $this->db->join("db_kec kc", "kc.id_kec = kl.id_kec");
        $data = $this->db->get_where("db_kelurahan kl", $where);
        return $data->result();
    }


    public function get_periksa_all(){
        $this->db->join("db_kec kc", "dpn.kec = kc.id_kec");
        $this->db->join("db_kelurahan kl", "dpn.kel = kl.id_kel");
        $this->db->join("db_kab kab", "dpn.kab = kab.id_kab");
        $data = $this->db->get("data_pasien_new dpn");
        return $data->result();
    }

    public function get_periksa_all_where($where){
        $this->db->join("db_kec kc", "dpn.kec = kc.id_kec");
        $this->db->join("db_kelurahan kl", "dpn.kel = kl.id_kel");
        $this->db->join("db_kab kab", "dpn.kab = kab.id_kab");
        $this->db->order_by("tgl_daftar", "desc");
        $data = $this->db->get_where("data_pasien_new dpn", $where);
        return $data->result();
    }

    public function get_pemeriksaan_all_where($where){
        $this->db->join("data_pasien_new dpn", "dpn.id_data = tp.id_pasien");
        $this->db->join("db_kec kc", "dpn.kec = kc.id_kec");
        $this->db->join("db_kelurahan kl", "dpn.kel = kl.id_kel");
        $this->db->join("db_kab kab", "dpn.kab = kab.id_kab");
        $this->db->order_by("waktu", "desc");
        $data = $this->db->get_where("tbl_periksa tp", $where);
        return $data->result();
    }

    public function get_pemeriksaan_all_where_xx($where){
        $this->db->join("data_pasien_new dpn", "dpn.id_data = tp.id_pasien");
        $this->db->join("db_kec kc", "dpn.kec = kc.id_kec");
        $this->db->join("db_kelurahan kl", "dpn.kel = kl.id_kel");
        $this->db->join("db_kab kab", "dpn.kab = kab.id_kab");
        $this->db->join("tbl_pejabat tpj", "tpj.id_pejabat = tp.id_pejabat");
        $data = $this->db->get_where("tbl_periksa tp", $where);
        return $data->row_array();
    }

    public function get_iden_all_where_xx($where){
        $this->db->join("db_kec kc", "dpn.kec = kc.id_kec");
        $this->db->join("db_kelurahan kl", "dpn.kel = kl.id_kel");
        $this->db->join("db_kab kab", "dpn.kab = kab.id_kab");
        $data = $this->db->get_where("data_pasien_new dpn", $where);
        return $data->row_array();
    }

}
?>